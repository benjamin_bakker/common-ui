/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Platform } from 'react-native';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color, flexbox, layout,
} from 'styled-system';

import Theme from '../../theme';

export const verticalOffsets = { ios: (Theme.sizes.textInputHeight_raw * 2) + 15, android: 0 };
export const behaviors = { ios: 'position', android: '' };

const StyledKeyboardAvoidingView = styled.KeyboardAvoidingView`
  ${color}
  ${flexbox}
  ${layout}
`;

export default function KeyboardAvoidingView({ noWrapTheme, ...props }) {
  const behavior = Platform.OS === 'ios' ? behaviors.ios : behaviors.android;
  const keyboardVerticalOffset = Platform.OS === 'ios' ? verticalOffsets.ios : verticalOffsets.android;

  const testProps = {
    testBehavior: behavior,
    testKeyboardVerticalOffset: keyboardVerticalOffset,
  };

  const content = (
    <StyledKeyboardAvoidingView
      {...props}
      {...testProps}
      behavior={behavior}
      keyboardVerticalOffset={keyboardVerticalOffset}
    />
  );

  // eslint-disable-next-line react/jsx-props-no-spreading
  if (noWrapTheme) return content;
  return (
    <ThemeProvider theme={Theme}>
      {content}
    </ThemeProvider>
  );
}

KeyboardAvoidingView.defaultProps = {
  backgroundColor: 'background',
  contentContainerStyle: { backgroundColor: Theme.colors.background },
};

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  flexbox,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme';

export const testBoxID = 'box';

const StyledView = styled.View.attrs(A11y)`
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${flexbox}
`;

function Box({
  noWrapTheme = false, ...props
}) {
  const extraProps = { testID: testBoxID, pointerEvents: 'box-none' };
  const { onPress } = props;

  if (onPress) {
    delete extraProps.pointerEvents;
  }

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledView {...extraProps} {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledView {...extraProps} {...props} />
    </ThemeProvider>
  );
}

Box.defaultProps = {};

export default Box;

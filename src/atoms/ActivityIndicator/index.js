// eslint-disable-next-line no-unused-vars
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import { variant } from 'styled-system';

import Theme from '../../theme';

const activityIndicatorVariants = variant({ scale: 'activityIndicator' });
const StyledActivityIndicator = styled.ActivityIndicator.attrs((props) => {
  const { color, size } = activityIndicatorVariants(props);
  return {
    color,
    size,
  };
})``;

function ActivityIndicator({ noWrapTheme = false, ...props }) {
  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledActivityIndicator {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledActivityIndicator {...props} />
    </ThemeProvider>
  );
}

ActivityIndicator.defaultProps = {};

export default ActivityIndicator;

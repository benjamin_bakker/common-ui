// eslint-disable-next-line no-unused-vars
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  typography,
  variant,
  system,
  flexbox,
  fontFamily,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme';

const textDecoration = system({ textDecoration: true });
const textDecorationColor = system({ textDecorationColor: true });
const textVariant = variant({ scale: 'text' });
const StyledText = styled.Text.attrs(A11y)`
  ${textVariant}
  ${textDecoration}
  ${textDecorationColor}
  ${color}
  ${space}
  ${typography}
  ${flexbox}
  ${fontFamily}
`;

function Text({ noWrapTheme = false, touchable = false, ...props }) {
  const propsCopy = { ...props };
  if (touchable) delete propsCopy.pointerEvents;

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledText {...propsCopy} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledText {...propsCopy} />
    </ThemeProvider>
  );
}

Text.defaultProps = {
  variant: 'p',
  touchable: false,
  pointerEvents: 'none',
};

export default Text;

// eslint-disable-next-line no-unused-vars
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';

import {
  borders, color, flexbox, layout, space,
} from 'styled-system';
import A11y from 'accessible-system';
import Theme from '../../theme';

const StyledHorizontalRule = styled.View.attrs(A11y)`
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${flexbox}
`;

function HorizontalRule({ noWrapTheme, ...props }) {
  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledHorizontalRule {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledHorizontalRule {...props} />
    </ThemeProvider>
  );
}

HorizontalRule.defaultProps = {
  borderBottomColor: Theme.colors.grey,
  borderBottomWidth: '1px',
  width: '100%',
  marginTop: '16px',
  marginBottom: '16px',
};

export default HorizontalRule;

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  flexbox,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme';

export const testTouchableID = 'touchable';

const StyledTouchable = styled.TouchableOpacity.attrs(A11y)`
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${flexbox}
`;

function Touchable({ noWrapTheme = false, ...props }) {
  const extraProps = { testID: testTouchableID };

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledTouchable {...extraProps} {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledTouchable {...extraProps} {...props} />
    </ThemeProvider>

  );
}

Touchable.defaultProps = {
  activeOpacity: 0.9,
};

export default Touchable;

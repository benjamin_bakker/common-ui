/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  flexbox,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme';

export const testScrollableID = 'scrollable';

const StyledScrollView = styled.ScrollView.attrs(A11y)`
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${flexbox}
`;

function Scrollable({
  noWrapTheme = false, contentContainerStyle = null, ...props
}) {
  const extraProps = { testID: testScrollableID };
  extraProps.contentContainerStyle = contentContainerStyle || {
    paddingBottom: 50,
  };

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledScrollView {...extraProps} {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledScrollView {...extraProps} {...props} />
    </ThemeProvider>

  );
}

Scrollable.defaultProps = {};

export default Scrollable;

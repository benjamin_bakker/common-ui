import Theme from './theme';

import ActivityIndicator from './atoms/ActivityIndicator';
import Box from './atoms/Box';
import Touchable from './atoms/Touchable';
import Scrollable from './atoms/Scrollable';
import Text from './atoms/Text';
import KeyboardAvoidingView from './atoms/KeyboardAvoidingView';
import HorizontalRule from './atoms/HorizontalRule';

import Accordion from './molecules/Accordion';
import Avatar from './molecules/Avatar';
import Button from './molecules/Button';
import ConversationAlertBubble from './molecules/ConversationAlertBubble';
import TextInput from './molecules/TextInput';
import SelectBox from './molecules/SelectBox';
import MessageBubble from './molecules/MessageBubble';
import MessageSendButton from './molecules/MessageSendButton';
import DashboardWidget from './molecules/DashboardWidget';
import CalendarMenuAction from './molecules/CalendarMenuAction';
import ColorBubblePicker from './molecules/ColorBubblePicker';

import LessonSummaryHeader from './organisms/LessonSummaryHeader';
import ConversationCard from './organisms/ConversationCard';
import ProfileCompletionWidget from './organisms/ProfileCompletionWidget';
import ResponseTimeWidget from './organisms/ResponseTimeWidget';
import BookingInSeasonWidget from './organisms/BookingInSeasonWidget';
import ProfileQualityWidget from './organisms/ProfileQualityWidget';
import ProfileRankWidget from './organisms/ProfileRankWidget';
import MonthViewCalendar from './organisms/MonthViewCalendar';
import OrganiserViewCalendar from './organisms/OrganiserViewCalendar';
import CalendarMenu from './organisms/CalendarMenu';

import MessageThread from './templates/MessageThread';

import useCurrencyFormatter from './hooks/useCurrencyFormatter';

import { commonValidators as validators } from './util/functions';
import { getDifferenceInHours, percentageOfHour } from './util/calendar';

import * as CalendarMocks from './mocks/calendar';

import * as CalendarQueryTemplates from './graphQL/queries/calendar';

export {
  Theme,

  ActivityIndicator,
  Box,
  Touchable,
  Scrollable,
  Text,
  KeyboardAvoidingView,
  HorizontalRule,

  Accordion,
  Avatar,
  Button,
  ConversationAlertBubble,
  TextInput,
  SelectBox,
  MessageBubble,
  MessageSendButton,
  DashboardWidget,
  CalendarMenuAction,
  ColorBubblePicker,

  LessonSummaryHeader,
  ConversationCard,
  ProfileCompletionWidget,
  ResponseTimeWidget,
  BookingInSeasonWidget,
  ProfileQualityWidget,
  ProfileRankWidget,
  MonthViewCalendar,
  OrganiserViewCalendar,
  CalendarMenu,

  MessageThread,

  useCurrencyFormatter,

  validators,
  getDifferenceInHours,
  percentageOfHour,

  CalendarMocks,

  CalendarQueryTemplates,
};

export enum PaymentProviders {
  VISA = 'visa',
  AMEX = 'amex',
  MASTER_CARD = 'masterCard',
  CB = 'cb',
  APPLE_PAY = 'apple',
  GOOGLE_PAY = 'google',
  NEW = 'New Card',
}

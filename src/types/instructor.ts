import { Image } from "./image";
import { Currency } from "./currency";

interface UserInteraction {
  count: number;
  type: string;
}

export interface TaskLink {
  text?: string;
  url: string;
}

export interface CompletionTask {
  name: string;
  completed: boolean;
  link?: TaskLink;
}

type Avatar = Image;

export enum UserType {
  CUSTOMER = "Customer",
  INSTRUCTOR = "Instructor",
  ADMIN = 'Admin',
}

export interface User {
  __typename: UserType;
  id: string;
  email: string;
  displayName: string;
  firstName?: string;
  lastName?: string;
  avatar?: Partial<Avatar>;
  phone?: string;
  impersonatedBy?: User;
  interactions: UserInteraction[];
}

export interface InstructorScoreHistory{
  date: string;
  total: number;
  resortRank: number;
  resortInstructorsCount: number;
}

export interface InstructorScore{
  conversionRateScore: number;
  responseTimeScore: number;
  refusalRateScore: number;
  profileQualityScore: number;
  reviewCountScore: number;
  reviewRateScore: number;
  bonusScore: number;
  bonusScoreDecayRate: number;
  total: number;
  history: InstructorScoreHistory[];
}

export interface ProfileCompletion {
  tasks: CompletionTask[];
  percentage: number;
}

export interface Profile {
  id: string;
  completion: ProfileCompletion;
  responseTime: number;
  inSeasonBookingsValue: number;
  currency: Currency;
  score: InstructorScore;
  instantBook: boolean;
  hasMyBusiness: boolean;
}

export interface Instructor extends User {
  profile: Profile;
}

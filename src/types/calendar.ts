import { BookingStatus, Resort, BookingSummary, Slot as BookingSlotType, SlotType } from './booking';
import { Currency } from './currency';
import { Customer } from './user';

export interface CalendarUtilsDay {
    /**
     * Month or Day number
     */
    date: number;
    /**
     * Readable date-string
     */
    iso: string;
    /**
     * Metadata to show if is previous, current, or next month date.
     */
    type: 'previous' | 'current' | 'next'
}

/**
 * Typing for 'calendar-dates' module 'getMatrix() method;
 */
export type CalendarUtilsMatrix = CalendarUtilsDay[][];

export interface Slot extends Omit<BookingSlotType, 'start' | 'end'> {
    start: Date;
    end: Date;
    isFullDay: boolean;

    type: SlotType;

    customer?: Customer;
    booking?: BookingSummary;

    title?: string;
    link?: string;
    extService?: string;
}

export interface SlotWithStatus extends Slot {
    status: BookingSummary['status'];
}

export type TimeRule = 'am' | 'pm' | 'fd';
export interface Rule {
    id: number;
    name: string;
    color: string;
    times: TimeRule[];
    minLessonsPerWeekday: number[];
}

export interface DayPricing {
    perHourMorning: number;
    perHourAfternoon: number;
    perDay: number;
}

export interface BookableSlot {
    start: string;
    end: string;
    isFullDay: boolean;
    isAfternoon: boolean;
    isEnabled: boolean;
}

export interface Day extends Omit<CalendarUtilsDay, 'iso'>, Omit<BookingSummary, 'instructor'|'slots'|'currency'>, BookingStatus {
    /**
     * During data-weaving, CalendarUtilsDay['iso'] gets cast to a Date;
     */
    iso: Date;

    /**
     * date string formatted to yyyy-mm-dd
     * This field is used as an attachment point to weave the MS-Api data with the CalendarUtilsMatrix data.
     */
    day: string;

    slots: Slot[];
    bookableSlots: BookableSlot[];
    resorts: Resort[];

    isBlocked: boolean;

    rule?: Rule;
    pricing: DayPricing;
}

export interface Week {
    days: Day[];
}

export type ExternalWeeks = Day[];
export interface Weeks {
    [weekIndex: number]: Week;
}

export interface BaseBubbleProps {
    start: Date;
    end: Date;
    isFullDay?: Slot['isFullDay'];

    isFirstBubble: boolean;
    color?: string;
    onPress: () => void;
    children: any;
}
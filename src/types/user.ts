import { Image } from "./image";
import {Currency} from "./currency";

type Avatar = Image;

export enum UserType {
  CUSTOMER = "Customer",
  INSTRUCTOR = "Instructor",
  ADMIN = 'Admin',
}

export type UserInteraction = {
  type: string;
  count: number;
}

export interface User {
  __typename: UserType;
  id: string;
  email: string;
  displayName: string;
  firstName?: string;
  lastName?: string;
  avatar?: Partial<Avatar>;
  phone?: string;
  impersonatedBy?: User;
  interactions: UserInteraction[];
}

export interface Customer extends User{
}

export interface Instructor extends User{
  profile: Partial<Profile>;
}

export interface Admin extends User{

}

export interface Profile{
  publicId: string;
  id: number;
  instantBook: boolean;
  hasMyBusiness: boolean;
  responseTime: number;
  currency: Currency;
  inSeasonBookings: number;
  inSeasonBookingsValue: number;
  score: InstructorScore;
  completion: ProfileCompletion;
}
export interface InstructorScore{
  conversionRateScore: number;
  responseTimeScore: number;
  refusalRateScore: number;
  profileQualityScore: number;
  reviewCountScore: number;
  reviewRateScore: number;
  bonusScore: number;
  bonusScoreDecayRate: number;
  total: number;
  history: InstructorScoreHistory[];
}

export interface InstructorScoreHistory{
  date: string;
  total: number;
  resortRank: number;
  resortInstructorsCount: number;
}
export interface ProfileCompletion{
  tasks: ProfileTask[];
  percentage: number;
}

export interface ProfileTask{
  name: string;
  completed: boolean;
}

import { PaymentProviders } from './paymentProviders';
import { User, Instructor, Customer} from './user';
import {MessageThread} from "./thread";
import { Currency } from './currency';

export enum Interval {
  HOUR = "HOUR",
  DAY = "DAY"
}

export enum SlotType {
  BOOKING = "BOOKING",
  ENQUIRY = "ENQUIRY",
  EXTERNAL = "EXTERNAL",
  REQUESTED_BOOKING = "REQUESTED_BOOKING",
};
export interface Slot {
  __typename: string;
  id: string;
  start: string;
  end: string;
  costPerInterval: number;
  interval: Interval;
  intervals: number;
  isFullDay: boolean;
  type: SlotType;
  link?: string;
  extService?: string;
  bookingSummary?: BookingSummary;
  customer?: Customer;
}

export interface Rates {
  day: number;
  hour: number;
}
export type SchoolRates = Rates;

export enum UIState {
  PRIMARY = "primary",
  SECONDARY = "secondary",
  TERTIARY = "tertiary",
  ATTENTION = "attention"
}

interface ActionLinkURI {
  __typename: 'URI';
  uri: string;
}
interface ActionLinkRouteLink {
  __typename: 'RouteLink';
  name: string;
  parameters: string;
}
interface ActionLinkResourceLink {
  __typename: 'ResourceLink';
  id: string;
  type: string;
}
interface ActionLinkNestedResourceLink {
  __typename: 'NestedResourceLink';
  parent: ActionLinkResourceLink;
  id: string;
  type: string;
}

export type ActionLink =
  ActionLinkURI
  |ActionLinkRouteLink
  |ActionLinkResourceLink
  |ActionLinkNestedResourceLink

export interface LessonAction {
  text: string;
  link: ActionLink;
  state: UIState;
}

export interface PaymentSummary {
  subTotal: number
  total: number;
  msFees: number;
  paymentFees: number;
  discount: Discount;
  amountDue: number;
  amountPaid: number;
  amountDueBy: Date;
  nextChargeAt: Date;
}

export interface CartInstructor {
  id: string;
  email: string;
  phone?: string;
  displayName: string;
  avatar?: {
    url: string;
  }
  profile: {
    publicId: string;
    id: number;
    instantBook: boolean;
  }
}

export interface Resort {
  name: string;
  slug: string;
  country?: string;
  schoolRates?: Rates
}

export enum Sport {
  SNOWBOARDING = "SNOWBOARDING",
  SKIING = "SKIING",
  CROSS_COUNTRY_SKIING = "CROSS_COUNTRY_SKIING",
  SKI_TOURING = "SKI_TOURING",
  TELEMARK = "TELEMARK"
}

export interface Discount {
  code: string;
  name: string;
  description: string;
  amount: number;
  percentage: number;
  removable: boolean;
}

export enum CancellationPolicy {
  SUPER_FLEXIBLE = "SUPER_FLEXIBLE",
  FLEXIBLE = "FLEXIBLE",
}

export enum AbilityLevel {
  BEGINNER = "BEGINNER",
  INTERMEDIATE = "INTERMEDIATE",
  ADVANCED = "ADVANCED",
  EXPERT = "EXPERT",
  OFF_PISTE = "OFF_PISTE",
  MIXED = "MIXED"
}

export enum ParticipantAge {
  CHILDREN = "CHILDREN",
  ADULTS = "ADULTS",
  MIXED = "MIXED"
}

export interface Cart {
  resort: Resort;
  sport: Sport;
  abilityLevel: AbilityLevel;
  ageOfParticipants: ParticipantAge;
  cancellationPolicy: CancellationPolicy;
  currency: string;
  instructor: CartInstructor;
  customer?: User
  slots: Slot[];
  numberOfParticipants: number;
  paymentSummary: PaymentSummary;
  amountToPay?: number;
  promoCode?: string;
  isProtected: boolean;
  language: string;
  bookingId?: string;
}

export enum TransactionType {
  PAYMENT = "PAYMENT",
  REFUND = "REFUND",
}
export interface Transaction {
  id: string;
  kind: TransactionType;
  description?: string;
  hasSucceeded: boolean;
  amount: number;
  currency: Currency;
  createdAt: string;
  updatedAt: Date;
  paymentMethod: PaymentMethod;
}

export enum BookingStatusState {
  CANCELLED = "CANCELLED",
  COMPLETED = "COMPLETED",
  CONFIRMED = "CONFIRMED",
  DECLINED = "DECLINED",
  EXPIRED = "EXPIRED",
  REQUESTED = "REQUESTED",
}

export enum PaymentStatusState {
  PAID = "PAID",
  PARTIAL = "PARTIAL",
  PARTIALLYREFUNDED = "PARTIALLYREFUNDED",
  REFUNDED = "REFUNDED",
  UNPAID = "UNPAID",
}

interface Status {
  enteredAt: string;
  by?: User;
}

export interface PaymentStatus extends Status {
  state: PaymentStatusState;
}

export interface BookingStatus extends Status {
  state: BookingStatusState;
}

export interface Review {
  id: string;
}

export interface BookingSummary{
  resort: Resort;
  sport: Sport;
  abilityLevel: AbilityLevel;
  ageOfParticipants: ParticipantAge;
  cancellationPolicy: CancellationPolicy;
  currency: Currency;
  instructor: Instructor;
  customer: Customer;
  slots: Slot[];
  numberOfParticipants: number;
  paymentSummary: PaymentSummary;
  paymentStatus: PaymentStatus;
  isProtected: boolean;
  language: string;
  id: string;
  createdAt: string;
  referenceNumber: string;
  status: BookingStatus;
  actions: LessonAction[];
  transactionHistory: Transaction[]|[];
  review: Review|null;
  thread: MessageThread|null;
}

interface CardData {
  last4: number;
  bankName: string;
  provider: PaymentProviders;
  expiryMonth: number;
  expiryYear: number;
}

export interface PaymentMethod {
  id: string;
  isDefault: boolean;
  paymentProvider: string;
  remoteId: string;
  createdAt: string;
  data: CardData;
}

export type TranslatorFunc = (str: string) => string;
export type I18nLocale = string;
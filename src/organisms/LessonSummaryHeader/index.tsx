import React from 'react';
import { Image } from 'react-native';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';

import Avatar from '../../molecules/Avatar';

export const lessonSummaryHeaderContainerTestID = 'lesson-summary-header-container';
export const chevronTestID = 'lesson-summary-chevron-container';

export interface LessonSummaryProps {
  title: string;
  subtitle: string;
  customerName: string;
  customerEmail: string;
  customerAvatarURL?: string;
  onPress?: () => void;
}

function LessonSummaryHeader({
  title = '',
  subtitle = '',
  customerName = '',
  customerEmail = '',
  customerAvatarURL,
  onPress,
}: LessonSummaryProps): any {
  return (
    <ThemeProvider theme={Theme}>
      <Touchable
        testID={lessonSummaryHeaderContainerTestID}
        noWrapTheme
        width={1}
        height={Theme.sizes.messageBookingHeaderHeight_raw}
        flexDirection="row"
        padding={3}
        backgroundColor="background"
        borderBottomWidth="thin"
        borderColor="grey"
        alignItems="center"
        activeOpacity={!!onPress ? 0.2 : 1}
        {...{onPress}}
      >
        <Avatar name={customerName} email={customerEmail} url={customerAvatarURL} />
        <Box noWrapTheme height={1} flex={1} paddingLeft={3} justifyContent="center">
          <Text noWrapTheme fontWeight={6} fontSize={2}>
            {title}
          </Text>
          <Text noWrapTheme fontSize={2}>{subtitle}</Text>
        </Box>
        {!!onPress &&(
          <Box testID={chevronTestID} noWrapTheme height={1} marginRight={3} alignItems="center" justifyContent="center">
            <Image source={require('../../assets/images/chevron.png')} style={{ width: 15, height: 15 }} />
          </Box>
        )}
      </Touchable>
    </ThemeProvider>
  );
}

export default LessonSummaryHeader;

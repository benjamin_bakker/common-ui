import React, { ReactNode } from 'react';

import Text from '../../atoms/Text';

import CommonDashboardWidget from '../../molecules/DashboardWidget';

import { Instructor } from '../../types/instructor';
import { TranslatorFunc } from '../../types/global';

interface Props {
  instructor: Instructor;
  width?: number;
  t: TranslatorFunc;
  DashboardWidget: (props: any) => React.ReactElement;
  MarkdownComponent: (props: any) => React.ReactElement;
}

const ProfileQualityWidget = ({
  instructor,
  width = 100,
  t,
  DashboardWidget = CommonDashboardWidget,
  MarkdownComponent = () => <></>,
}: Props) => {
  const score = instructor.profile?.score?.profileQualityScore || 0;

  const collapsed = (
    <MarkdownComponent children={t('widget.profile-quality.description')}/>
  )

  return (
    <DashboardWidget
      title={t('widget.profile-quality.title')}
      collapsedContent={collapsed}
      width={width}
    >
      <Text variant="h2" display="flex" alignItem="center">{score}/100</Text>
    </DashboardWidget>
  );
}

export default ProfileQualityWidget;

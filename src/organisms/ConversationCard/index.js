/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';

import Avatar from '../../molecules/Avatar';
import { getFormattedTimeSent } from '../../util/dateTime';

const colorsByStatus = {
  enquiry: 'primary',
  unpaid: 'orange',
  cancelled: 'red',
  confirmed: 'green',
};

export const conversationCardTimeSentTestID = 'conversation-card-time-sent';

function ConversationCard({
  index,
  name,
  latestMessage,
  timeSent,
  totalUnread,
  bookingStatus,
  onPress,
  email,
  userAvatarUrl,
  t,
}) {
  const isOdd = !!(index % 2);
  const moreThan100Messages = totalUnread >= 100;
  const sharedTextProps = {};
  const formattedTimeSent = timeSent ? getFormattedTimeSent(timeSent, t) : null;

  if (totalUnread) sharedTextProps.fontWeight = 'bold';
  return (
    <>
      <Touchable
        width={1}
        height={110}
        flexDirection="row"
        alignItems="center"
        backgroundColor={isOdd ? 'convoPink' : 'convoGrey'}
        borderRadius={8}
        marginBottom={3}
        padding={3}
        onPress={onPress}
      >
        <Avatar name={name} email={email} url={userAvatarUrl} />
        <Box noWrapTheme height={1} flex={1} paddingLeft={3} justifyContent="center">
          <Box width="100%" flexDirection="row" alignItems="center" justifyContent="space-between">
            <Box noWrapTheme height={1} maxWidth="75%" flexDirection="row" alignItems="center" justifyContent="center">
              <Text
                noWrapTheme
                {...sharedTextProps}
                fontSize={2}
                marginRight={2}
                numberOfLines={1}
              >
                {name}
              </Text>
              {bookingStatus && (
                <Box noWrapTheme width="15px" height="15px" borderRadius="15px" backgroundColor={colorsByStatus[bookingStatus]} />
              )}
            </Box>
            <Text
              testID={conversationCardTimeSentTestID}
              noWrapTheme
              {...sharedTextProps}
              marginLeft={3}
            >
              {formattedTimeSent}
            </Text>
          </Box>
          <Box flexDirection="row" alignItems="center" justifyContent="space-between">
            <Box flex={totalUnread ? 0.85 : 1}>
              <Text noWrapTheme {...sharedTextProps} fontSize={1} numberOfLines={2}>
                {latestMessage}
              </Text>
            </Box>
            {!!totalUnread && (
              <Box flex={0.15} alignItems="center">
                <Box
                  noWrapTheme
                  width="25px"
                  height="25px"
                  borderRadius="20px"
                  backgroundColor="dustyBlue"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Text noWrapTheme {...sharedTextProps} fontSize={moreThan100Messages ? '10px' : 0} color="white" padding={0}>{moreThan100Messages ? '99+' : totalUnread}</Text>
                </Box>
              </Box>
            )}

          </Box>
        </Box>
      </Touchable>
    </>
  );
}

ConversationCard.defaultProps = {
  latestMessage: '',
  timeSent: null,
  totalUnread: 0,
  bookingStatus: null,
  onPress: () => {},
  email: '',
  userAvatarUrl: '',
  t: (key) => key,
};

ConversationCard.propTypes = {
  index: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  latestMessage: PropTypes.string,
  timeSent: PropTypes.string,
  totalUnread: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  bookingStatus: PropTypes.string,
  onPress: PropTypes.func,
  email: PropTypes.string,
  userAvatarUrl: PropTypes.string,
  t: PropTypes.func,
};

export default ConversationCard;

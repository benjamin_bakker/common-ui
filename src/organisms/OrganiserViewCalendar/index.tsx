import React from 'react';
import { areIntervalsOverlapping } from 'date-fns';
import md5 from 'md5';

import Box from '../../atoms/Box';
import Scrollable from '../../atoms/Scrollable';

import BookingRuleBanner from '../../molecules/OrganiserBookingRuleBanner';
import SlotBubble, { BubbleStyleProps } from '../../molecules/OrganiserSlotBubble';
import { Props as MonthViewProps } from '../MonthViewCalendar';

import { TIME_BLOCK_HEIGHT, VISIBLE_HOURS } from '../../constants/calendar';

import { Day, Slot, SlotWithStatus } from '../../types/calendar';
import DateHeading from '../../molecules/OrganiserDateHeading';
import Text from '../../atoms/Text';
import { TranslatorFunc } from '../../types/global';
import { SlotType } from '../../types/booking';

interface WeavedSlot extends BubbleStyleProps, Slot {
}
interface Props {
    day: Day;
    onViewSlot: (slot: Slot) => any;
    renderMenu?: MonthViewProps['renderMenu'];
    t: TranslatorFunc;
}

function chunkOverlap(slotsToCheckAgainst: WeavedSlot[], slot: Slot) {
    return (
        slotsToCheckAgainst
            .some(
                (_slot) => areIntervalsOverlapping(
                    { start: _slot.start, end: _slot.end },
                    { start: slot.start, end: slot.end },
                )
            )
    );
}

const OrganiserView = (props: Props) => {
    if (!props.day) return null;

    const slotsChunkedForOverlap: WeavedSlot[][] = (
        props.day.slots
            .sort((a, b) => a.start > b.start ? 1 : -1)
            .reduce((acc: WeavedSlot[][], slot) => {
                const _acc = [...acc];
                const prev: WeavedSlot[] | undefined = _acc[_acc.length - 1];

                if (prev) {
                    if (chunkOverlap(prev, slot)) {
                        _acc[_acc.length - 1].push(slot);
                    } else {
                        _acc.push([slot]);
                    }
                }
                else {
                    _acc.push([slot]);
                }

                return _acc;
            }, [])
    );

    return (
        <Box backgroundColor="white" flex={1} width={1} style={{ position: 'relative' }}>
            <DateHeading t={props.t} date={props.day.iso} />
            <Scrollable
                padding={2}
                paddingRight={0}
                flex={1}
            >
                <BookingRuleBanner t={props.t} {...props.day.rule} />
                <Box marginTop={3} style={{ position: 'relative' }}>
                    <Box>
                        {VISIBLE_HOURS.map((hr) => {
                            return (
                                <Box key={hr} height={TIME_BLOCK_HEIGHT}>
                                    <Box flexDirection="row" alignItems="center">
                                        <Box justifyContent="center" minWidth="20%">
                                            <Text color="textGrey" fontWeight={5} marginRight={3}>{hr}:00</Text>
                                        </Box>
                                        <Box width="80%" flex={1} borderTopWidth={1 / 2} borderColor="darkGrey" />
                                    </Box>
                                </Box>
                            );
                        })}
                    </Box>
                    <Box flexDirection="row" width={1} style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0 }}>
                        <Box justifyContent="center" minWidth="20%" />
                        <Box flex={1}>
                            {
                                slotsChunkedForOverlap
                                    .map((slotChunks: WeavedSlot[]) => {
                                        return slotChunks.map((slot: SlotWithStatus, i) => {
                                            let leftPosition: number = (100 / slotChunks.length) * i;
                                            if (i === 0) leftPosition = 0;

                                            if (slot.type === SlotType.EXTERNAL) {
                                                return (
                                                    <SlotBubble.External
                                                        key={md5(JSON.stringify(slot))}
                                                        onPress={props.onViewSlot}

                                                        t={props.t}

                                                        {...slot}

                                                        isFirstBubble={i === 0}

                                                        left={`${leftPosition}%`}
                                                        width={slotChunks.length}
                                                        small={slotChunks.length > 2}
                                                        fullyRounded={slotChunks.length > 1}
                                                    />
                                                );
                                            }

                                            if (slot.type === SlotType.REQUESTED_BOOKING) {
                                                return (
                                                    <SlotBubble.BookingRequest
                                                        key={md5(JSON.stringify(slot))}
                                                        onPress={props.onViewSlot}

                                                        t={props.t}

                                                        {...slot}

                                                        isFirstBubble={i === 0}

                                                        left={`${leftPosition}%`}
                                                        width={slotChunks.length}
                                                        small={slotChunks.length > 2}
                                                        fullyRounded={slotChunks.length > 1}
                                                    />
                                                );
                                            }

                                            return (
                                                <SlotBubble.Booking
                                                    key={md5(JSON.stringify(slot))}
                                                    onPress={props.onViewSlot}

                                                    t={props.t}

                                                    {...slot}

                                                    isFirstBubble={i === 0}

                                                    left={`${leftPosition}%`}
                                                    width={slotChunks.length}
                                                    small={slotChunks.length > 2}
                                                    fullyRounded={slotChunks.length > 1}
                                                />
                                            );
                                        });
                                    })
                            }
                        </Box>
                    </Box>
                </Box>
            </Scrollable>

            {props.renderMenu && props.renderMenu()}
        </Box>
    );
}

export default OrganiserView

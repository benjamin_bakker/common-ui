import React from 'react';

import Touchable from '../../atoms/Touchable';

interface Props {
    visible: boolean;
    onPress: () => void;
}

const Modal = (props: Props) => {
    if (!props.visible) return null;

    return (
        <Touchable
            onPress={props.onPress}
            backgroundColor="black"
            opacity={0.8}
            style={{
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            }}
        />
    )
}

export default Modal;

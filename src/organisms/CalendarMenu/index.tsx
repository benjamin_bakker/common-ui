import React from 'react';

import Box from '../../atoms/Box';
import Theme from '../../theme';

import MenuAction from '../../molecules/CalendarMenuAction';

import BackDrop from './BackDrop';

import { TranslatorFunc } from '../../types/global';

export type MenuActions = 'settings' | 'new';
interface Props {
    visible: boolean;
    hide?: MenuActions[];
    onActionPress: (action: MenuActions) => void;
    onToggleMenuVisible: (visible: boolean) => void;

    toggleActionVariant?: 'dark' | 'light';

    t: TranslatorFunc;
}

const Menu = (props: Props) => {
    const { hide = [] } = props;
    function _show() {
        props.onToggleMenuVisible(true);
    }
    function _hide() {
        props.onToggleMenuVisible(false);
    }

    return (
        <>
            <BackDrop onPress={_hide} visible={props.visible} />
            <Box style={{ position: 'absolute', bottom: Theme.space[3], right: Theme.space[3] }}>
                {props.visible ? (
                    <>
                        {!hide.includes('settings') && <MenuAction.Settings t={props.t} onPress={() => props.onActionPress('settings')} marginBottom={3} />}
                        {!hide.includes('new') && <MenuAction.Calendar t={props.t} onPress={() => props.onActionPress('new')} />}
                    </>
                ) : (
                    <MenuAction.Plus variant={props.toggleActionVariant} onPress={_show} />
                )}
            </Box>
        </>
    )
}

export default Menu;

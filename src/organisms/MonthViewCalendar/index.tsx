import React from 'react';
import { Image } from 'react-native';
import calendarDates from 'calendar-dates';
import {
    getMonth,
    getYear,
    isThisYear,
    subMonths,
    addMonths,
    isToday,
    format,
} from 'date-fns';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable'
import Text from '../../atoms/Text'

import WeekDayHeading, { Props as WeekDayHeadingProps } from '../../molecules/WeekDayHeading';

import Theme from '../../theme';

import { Props as DayCellProps } from '../../molecules/DayCell';
import LoadingOverlay from '../../molecules/CalendarLoadingOverlay';
import WeekRow from '../../molecules/WeekRow';

import { I18nLocale, TranslatorFunc } from '../../types/global';
import { CalendarUtilsMatrix, Day, ExternalWeeks, Week, Weeks } from '../../types/calendar';

import { MONTH_NAMES, MONTH_VIEW_MAX_WEEKS_SHOWN } from '../../constants/calendar';

import {
    _getMockDay,
    _getMockDayPricing,
    _getMockRule,
    _getMockSchedule,
} from '../../mocks/calendar';

import { _getRandomBool } from '../../mocks/common';
import { Currency } from '../../types/currency';

const CalendarUtils: { getMatrix: (date: Date) => Promise<CalendarUtilsMatrix> } = new calendarDates();

export const getPrevMonth = (currentMonth: Date, goBackBy: number = 1): Date => subMonths(currentMonth, goBackBy);
export const getNextMonth = (currentMonth: Date, goForwardBy: number = 1): Date => addMonths(currentMonth, goForwardBy);
export interface Props {
    onMonthChange: (monthInView: Date) => any;
    onDayColumnPress?: WeekDayHeadingProps['onDayPress'];
    onToggleCalendarView?: (currentView: DayCellProps['view']) => any;
    onDayPress: (day: Day) => any;
    /**
     * This method is to be used to set the selectedDay when loading the month-view.
     * A use-case would be to auto-populate the organizer-view with todays date upon initial load.
     */
    onLoadWithTodaySelected?: (day: Day) => void;

    /**
     * Once the Calendar has created it's Matrix for the monthInView
     * It will ask for data to weave into the Matrix (in our case the data will be in the form of @type ExternalWeeks)
     * Arguments start and end will be date-string in format yyyy-mm-dd.
     */
    onRequestForData: (start: string, end: string) => Promise<ExternalWeeks>;

    /**
     * If the calendars month data changes, we need to update any components outside of the
     * month-view so everything stays up to date.
     */
    onSelectedDayUpdate: (day: Day) => void;

    /** This allows the component to re-render (re-draw any new data) */
    refreshKey: number;
    currency: Currency;
    monthInView: Date;
    selectedDay: Date;
    calendarView: DayCellProps['view'];
    loading?: boolean;
    hideActions?: boolean;

    renderLeftControl?: () => React.ReactElement;
    renderRightControl?: () => React.ReactElement;
    renderMenu?: () => React.ReactElement;

    t: TranslatorFunc;
    locale: I18nLocale;
}

const MonthView = (props: Props) => {
    const { t, locale } = props;

    const hasAlreadyCalledOnLoadWithTodaySelected = React.useRef(false);

    const [monthSchedule, setMonthSchedule] = React.useState<Weeks>({});
    const [loadingDates, setLoadingDates] = React.useState(false)

    function handleChangeMonth(direction: 'next' | 'prev') {
        let newMonth: Date|null = null;

        if (direction === 'next') newMonth = getNextMonth(props.monthInView);
        if (direction === 'prev') newMonth = getPrevMonth(props.monthInView);

        if (newMonth) {
            props.onMonthChange(newMonth);
        }
    }

    function handleDayColumnPress(dayIndex: number, dayName: string): void {
        props.onDayColumnPress && props.onDayColumnPress(dayIndex, dayName);
    }

    React.useEffect(() => {
        async function setupMonth() {
            let firstDayOfMatrix: string|null = null;
            let lastDayOfMatrix: string|null = null;

            setLoadingDates(true);

            const _monthSchedule: Weeks = {};
            try {
                const matrix = await CalendarUtils.getMatrix(props.monthInView);
                matrix
                    .forEach((daysOfWeek, weekIndex) => {
                        _monthSchedule[weekIndex] = {
                            // @ts-ignore
                            days: daysOfWeek.map((day, dayIndex) => {
                                if (weekIndex === 0 && dayIndex === 0) firstDayOfMatrix = day.iso;
                                if (weekIndex === 4 && dayIndex === daysOfWeek.length - 1) lastDayOfMatrix = day.iso;

                                return {
                                    ...day,
                                    day: day.iso,
                                    iso: new Date(day.iso),
                                    slots: [],
                                };
                            }, {}),
                        };
                    });

                if (firstDayOfMatrix && lastDayOfMatrix) {
                    const externalData = await props.onRequestForData(firstDayOfMatrix, lastDayOfMatrix);

                    if (!externalData) return;

                    const schedule: Weeks = (
                        Object
                            .entries(_monthSchedule)
                            .reduce((acc, [weekIndex, week]) => {
                                return {
                                    ...acc,
                                    [weekIndex]: {
                                        ...week,
                                        days: week.days.map((day: Day) => {
                                            const rtn: Day = {
                                                ...day,
                                                ...(externalData.find(({ day: dayKey }) => day.day === dayKey) || {})
                                            };

                                            /**
                                             * If we already have a selected day and are reloading the month-view
                                             * we should send out any new data linked to the selected day to update
                                             * all other views outside of the month-view.
                                             */
                                            if (props.selectedDay && rtn.iso) {
                                                if (props.selectedDay.getTime() === day.iso.getTime()) {
                                                    props.onSelectedDayUpdate(rtn);
                                                }
                                            }

                                            return rtn;
                                        })
                                    }
                                };
                            }, _monthSchedule)
                    );

                    setMonthSchedule(schedule);

                    /**
                     * On initial load we show the current month;
                     * So we want to show the current day in the schedule view.
                    */
                    let today: Day|null = null;
                    for (const week of Object.values(schedule)) {
                        week as unknown as Week;

                        if (today) break;

                        const days: Day[] = Object.values(week.days);
                        const _today = days.find((day) => isToday(day.iso));
                        if (_today) today = _today;
                    }

                    if (today && props.onLoadWithTodaySelected && !hasAlreadyCalledOnLoadWithTodaySelected.current) {
                        props.onLoadWithTodaySelected(today);
                        hasAlreadyCalledOnLoadWithTodaySelected.current = true;
                    }
                }

            } catch (error) {
                console.error(error);
            } finally {
                setLoadingDates(false);
            }
        }

        setupMonth();
    }, [props.monthInView, props.refreshKey]);

    if (!monthSchedule) return <LoadingOverlay backgroundColor="white" opacity={1} />;

    return (
        <Box width={1} flex={1} style={{ position: 'relative' }}>
            <Box>
                <Box flexDirection="row" justifyContent="center" alignItems="center" marginTop={3}>
                    {props.renderLeftControl ? props.renderLeftControl() : (
                        <Box flex={0.1} padding={3} />
                    )}
                    <Touchable
                        disabled={props.loading || loadingDates}
                        onPress={() => handleChangeMonth('prev')}
                        flex={1}
                        marginRight={1}
                        style={{ transform: [{ rotate: '180deg' }] }}
                    >
                        <Image
                            source={require('../../assets/images/chevron.png')}
                            style={{
                                tintColor: Theme.colors.black,
                                width: 12.5,
                                height: 12.5,
                            }}
                        />
                    </Touchable>
                    <Box flex={1} justifyContent="center" alignItems="center">
                        <Text variant="h3" padding={0}>{t(MONTH_NAMES[getMonth(props.monthInView)])}</Text>
                    </Box>
                    <Touchable disabled={props.loading || loadingDates} onPress={() => handleChangeMonth('next')} flex={1} marginLeft={1}>
                        <Image
                            source={require('../../assets/images/chevron.png')}
                            style={{
                                tintColor: Theme.colors.black,
                                width: 12.5,
                                height: 12.5,
                            }}
                        />
                    </Touchable>
                    {props.renderRightControl ? props.renderRightControl() : (
                        <Box flex={0.1} backgroundColor="red" padding={3} />
                    )}
                </Box>
                <Text
                    variant="h4"
                    textAlign="center"
                    marginBottom={1}
                    color={isThisYear(props.monthInView) ? 'transparent' : 'textGrey'}
                    fontSize={0}
                >
                    {getYear(props.monthInView)}
                </Text>

                <WeekDayHeading locale={locale} onDayPress={handleDayColumnPress} />
            </Box>

            <Box flex={1}>
                {Object.entries(monthSchedule).map(([weekNumber, { days }]) => {
                    if (+weekNumber > MONTH_VIEW_MAX_WEEKS_SHOWN) return null;

                    return (
                        <WeekRow
                            locale={locale}
                            onViewDay={props.onDayPress}
                            key={weekNumber}
                            currency={props.currency}
                            {...{ weekNumber, days, view: props.calendarView, selectedDay: props.selectedDay }}
                        />
                    );
                })}
            </Box>

            {loadingDates && <LoadingOverlay />}

            {props.renderMenu && props.renderMenu()}
        </Box>
    )
}

export default MonthView;

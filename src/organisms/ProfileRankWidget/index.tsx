import React, { ReactElement } from 'react';
import { Image } from 'react-native';
import { Box } from '../..';

import Text from '../../atoms/Text';
import Theme from '../../theme';

import { InstructorScore, Profile } from '../../types/instructor';

type Delta = 'up'|'down'|'same'

interface Props {
  instructorProfileScoreHistory: Partial<InstructorScore['history']>;
  renderRankText: (text: string) => ReactElement;
}

const ProfileRankWidget = ({
  instructorProfileScoreHistory,
  renderRankText = () => <></>,
}: Props) => {
  const rank = instructorProfileScoreHistory[0]?.resortRank || 0;
  const resortInstructorsCount = instructorProfileScoreHistory[0]?.resortInstructorsCount || 0;
  const previous = instructorProfileScoreHistory[1]?.resortRank || 0;

  let delta: Delta = 'down';
  if (rank < previous) delta = 'up';
  if (rank === previous) delta = 'same';

  let rankText: string|number = rank;
  if (rank < 1) rankText = 'widget.profile-rank.insufficient';
  if (rank > (resortInstructorsCount / 2)) rankText = 'widget.profile-rank.bottom-50-percent';

  const UP = delta === 'up';
  const chevronAngle = UP ? '-90deg' : '90deg';

  return (
    <Box flexDirection="row" alignItems="center" margin={0} padding={0}>
      {renderRankText(String(rankText))}
      {delta !== 'same' && (
        <Image
          source={require('../../assets/images/chevron.png')}
          style={{
            width: 15,
            height: 15,
            marginLeft: Theme.space[3],
            transform: [{ rotate: chevronAngle }],
            tintColor: UP ? Theme.colors.green : Theme.colors.red,
          }}
        />
      )}
    </Box>
  );
}

export default ProfileRankWidget;

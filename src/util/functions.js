/* eslint-disable import/prefer-default-export */
import emojiRegex from 'emoji-regex';
import emailValidator from 'email-validator';
import equals from 'validator/lib/equals';
import isMobilePhone from 'validator/lib/isMobilePhone';

import Theme from '../theme';

const defaultColors = [
  Theme.colors.primary,
  Theme.colors.darkPrimary,
  Theme.colors.lightPrimary,
  Theme.colors.darkGrey,
  Theme.colors.dustyBlue,
  Theme.colors.red,
  Theme.colors.lightRed,
  Theme.colors.green,
  Theme.colors.darkGreen,
  Theme.colors.lightGreen,
];

export const debounceDefaults = {
  onChange: 300,
};

function stringAsciiPRNG(value, m) {
  const charCodes = [...value].map((letter) => letter.charCodeAt(0));
  const len = charCodes.length;

  const a = (len % (m - 1)) + 1;
  const c = charCodes.reduce((current, next) => current + next) % m;

  let random = charCodes[0] % m;
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < len; i++) random = ((a * random) + c) % m;

  return random;
}

export function getRandomColor(value, colors = defaultColors) {
  if (!value) return 'primary';

  const colorIndex = stringAsciiPRNG(value, colors.length);
  return colors[colorIndex];
}

export function isSingleEmoji(content) {
  if (typeof content !== 'string') return content;

  const reg = new RegExp(`^(${emojiRegex().source})$`);
  return content.trim().match(reg);
}

export const commonValidators = {
  email: emailValidator.validate,
  equals,
  isMobilePhone: (value, options = {}) => isMobilePhone(value, 'any', { strictMode: true, ...options }),
};

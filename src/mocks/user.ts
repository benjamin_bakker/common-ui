import Fakerator from 'fakerator';
import { Currency } from '../types/currency';

import { Customer, Instructor, UserType } from '../types/user';

import { _getRandomBool, _getRandomElementOfEnum, _getRandomId, _getRandomNumber } from './common';

const fakerator = Fakerator();

export function _getMockCustomer(): Customer {
    return {
        __typename: _getRandomElementOfEnum<UserType>(UserType),
        id: _getRandomId() as string,
        email: fakerator.internet.email(),
        displayName: fakerator.names.name(),
        firstName: fakerator.names.firstName(),
        lastName: fakerator.names.lastName(),
        avatar: {
            url: fakerator.internet.gravatar(),
        },
        phone: fakerator.phone.number(),
        interactions: [],
    };
}

export function _getMockInstructor(): Instructor {
    return {
        ..._getMockCustomer(),
        profile: {
            publicId: _getRandomId() as string,
            id: _getRandomId() as number,
            instantBook: _getRandomBool(),
            hasMyBusiness: _getRandomBool(),
            responseTime: _getRandomNumber(),
            currency: _getRandomElementOfEnum<Currency>(Currency),
            inSeasonBookings: _getRandomNumber(),
            inSeasonBookingsValue: _getRandomNumber(),
            score: {
                bonusScore: _getRandomNumber(),
                bonusScoreDecayRate: _getRandomNumber(),
                conversionRateScore: _getRandomNumber(),
                profileQualityScore: _getRandomNumber(),
                refusalRateScore: _getRandomNumber(),
                responseTimeScore: _getRandomNumber(),
                reviewCountScore: _getRandomNumber(),
                reviewRateScore: _getRandomNumber(),
                total: _getRandomNumber(),
                history: [],
            },
        }
    }
}
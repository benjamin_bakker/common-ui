export const _pickFromArray = (arr: any[] = [], amount: number = 5): any[] => {
    const shuffled = arr.sort(() => .5 - Math.random());

    return shuffled.slice(0, amount);
}


export const _getRandomBool = (): boolean => {
    const ZeroOrOne = Math.floor(Math.random() * (2 - 0 + 1)) + 0;
    const bools = [true, false];

    return bools[ZeroOrOne];
}

export const _getRandom = (...arr: any[]): any => {
    const index = Math.floor(Math.random() * ((arr.length - 1) - 0 + 1)) + 0;

    return arr[index];
}

export const _getRandomId = (isNumber?: boolean): string | number => isNumber ? Math.random() : Math.random().toString();
export const _getRandomNumber = (): number => {
    const min = Math.ceil(1);
    const max = Math.floor(10);
    return Math.floor(Math.random() * (max - min));
}

export function _getRandomElementOfEnum<E>(e: any): E {
    const keys = Object.keys(e);
    const index = Math.floor(Math.random() * keys.length);
    const k = keys[index];

    if (typeof e[k] === 'number') return <any>e[k];
    return <any>k;
}
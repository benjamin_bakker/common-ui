export const VISIBLE_HOURS: number[] = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
export const TIME_BLOCK_HEIGHT: number = 1000 / (VISIBLE_HOURS.length - 1);

export const FALLBACK_WEEK_DAYS: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
export const MONTH_NAMES: string[] = ['text.months.jan', 'text.months.feb', 'text.months.mar', 'text.months.apr', 'text.months.may', 'text.months.jun', 'text.months.jul', 'text.months.aug', 'text.months.sep', 'text.months.oct', 'text.months.nov', 'text.months.dec'];
export const AVAILABLE_SLOTS: number[][] = [[9, 11], [11, 13], [13, 15], [15, 17], [17, 18]];
export const RULE_COLORS: string[] = ['#fba9a6', '#aed3ff', '#d2abf5', '#b6f7b0', '#feffb2'];
export const TIMES_OF_DAY: string[] = ['am', 'pm', 'fd'];

export const MONTH_VIEW_CAL_BORDER_RADIUS = 10;
export const MONTH_VIEW_MAX_WEEKS_SHOWN = 4;
export const CALENDAR_GENERAL_BORDER_WIDTH = 0.25;
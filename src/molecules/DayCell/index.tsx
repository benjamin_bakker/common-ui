import React from 'react';
import { isSameDay, isToday } from 'date-fns';
import md5 from 'md5';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';

import Theme from '../../theme';

import useCurrencyFormatter from '../../hooks/useCurrencyFormatter';

import { I18nLocale } from '../../types/global';
import { Day } from '../../types/calendar';
import { SlotType } from '../../types/booking';
import { Currency } from '../../types/currency';

import { CALENDAR_GENERAL_BORDER_WIDTH } from '../../constants/calendar';

import SlotBubble from '../DayCellSlotBubble';

export interface Props extends Day {
    onViewDay: () => void;
    view: 'bookings' | 'pricing';
    selectedDay: Date;
    dayIndex: number;
    locale: I18nLocale;
    currency: Currency;
}

export const MAX_VIEWABLE_SLOTS = 2;
const SELECTED_COLOR = Theme.colors.darkPrimary;

const DayCell = ({
    rule,
    slots,
    view,
    isBlocked,
    type,
    date,
    iso,
    pricing,
    currency,
    onViewDay,
    selectedDay,
    locale,
}: Props) => {
    const { format } = useCurrencyFormatter({ locale });

    const dayIsBlocked = isBlocked || (type === 'next' || type === 'previous');
    const dayIsSelected = isSameDay(iso, selectedDay);
    const BOX_PROPS: any = {
        style: {
            outlineColor: Theme.colors.lightGrey,
            outlineStyle: "solid",
            outlineWidth: CALENDAR_GENERAL_BORDER_WIDTH/2,
        },
    };
    const TEXT_PROPS: any = {};
    const PRICE_TEXT_PROPS: any = {};

    if (isToday(iso)) {
        BOX_PROPS.backgroundColor = 'lightPrimary';
        TEXT_PROPS.color = 'white';
        TEXT_PROPS.fontWeight = 5;
        PRICE_TEXT_PROPS.color = 'white'
    }

    if (dayIsBlocked) {
        BOX_PROPS.backgroundColor = 'grey';
        TEXT_PROPS.color = 'textGrey';
    }

    if (dayIsSelected) {
        BOX_PROPS.backgroundColor = SELECTED_COLOR;
        TEXT_PROPS.color = 'white';
        PRICE_TEXT_PROPS.color = 'white'
    }

    const dotColor = dayIsBlocked && !dayIsSelected ? 'textGrey' : !!rule ? rule.color : null;

    return (
        <Touchable flex={1} minHeight="100%" onPress={onViewDay}>
            <Box
                flex={1}
                justifyContent="flex-start"
                {...BOX_PROPS}
            >
                <Box height="25%" justifyContent="center" alignItems="center">
                    <Text
                        textAlign="center"
                        fontSize={1}
                        fontWeight={6}
                        padding={0}
                        color="textGrey"
                        {...TEXT_PROPS}
                    >
                        {date}
                    </Text>

                    <Box
                        alignSelf="center"
                        width="5px"
                        height="5px"
                        borderRadius="2.5px"
                        backgroundColor={dotColor || 'transparent'}
                    />
                </Box>
                <Box height="75%" padding={1} paddingTop={0}>
                    {view === 'bookings' && (
                        slots
                            .sort((a, b) => a.start > b.start ? 1 : -1)
                            .map((slot) => {

                                if (slot.type === SlotType.EXTERNAL) {
                                    return <SlotBubble.External key={md5(JSON.stringify(slot))} {...slot} color={dayIsBlocked ? 'darkGrey' : void 0} />;
                                }

                                if (slot.type === SlotType.REQUESTED_BOOKING) {
                                    return (
                                        <SlotBubble.BookingRequest
                                            key={md5(JSON.stringify(slot))}
                                            {...slot}
                                            color={dayIsBlocked ? 'darkGrey' : void 0}
                                        />
                                    );
                                }

                                return (
                                    <SlotBubble.Booking
                                        key={md5(JSON.stringify(slot))}
                                        {...slot}
                                        color={dayIsBlocked ? 'darkGrey' : void 0}
                                    />
                                );
                            })
                    )}

                    {view === 'pricing' && (
                        <>
                            <Box flex={1}>
                                <Text
                                    fontSize={0}
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perHourMorning, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                            <Box flex={1}>
                                <Text
                                    fontSize={0}
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perHourAfternoon, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                            <Box flex={1}>
                                <Text
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perDay, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                        </>
                    )}
                </Box>
            </Box>
        </Touchable>
    );
}

export default DayCell;

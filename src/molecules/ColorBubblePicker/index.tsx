import React from 'react';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Theme from '../../theme';

type Hex = string;
interface Props {
    colors: Hex[];
    value: Hex;
    onChange: (hex: Hex) => void;
    bubbleSize?: number;
}

const DOT_SIZE = 30;

const ColorBubblePicker = ({ value, onChange, colors = [], bubbleSize = DOT_SIZE }: Props) => {
    return (
        <Box flexDirection="row">
            {colors.map((hex) => {
                const isSelected = value === hex;
                return (
                    <Touchable
                        key={hex}
                        onPress={() => onChange(hex)}
                        width={bubbleSize}
                        height={bubbleSize}
                        borderRadius={bubbleSize / 2}
                        borderWidth={1}
                        borderColor={isSelected ? 'black' : 'white'}
                        marginRight={2}
                        backgroundColor={hex}
                        style={{
                            shadowColor: Theme.colors.black,
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,

                            elevation: 5,
                        }}
                    />
                );
            })}
        </Box>
    )
}

export default ColorBubblePicker;

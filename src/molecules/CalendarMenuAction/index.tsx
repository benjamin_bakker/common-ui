import React from 'react';
import { Image } from 'react-native';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';

import Theme from '../../theme';

import { TranslatorFunc } from '../../types/global';

const DEFAULT_ACTION_SIZE = 50;

interface Props {
    onPress?: () => any;
    icon?: React.ReactElement;
    size?: number;
    variant?: 'light' | 'dark';
    label?: string;
    color?: string;
    [key: string]: any;
}

interface PropsWithTranslator extends Props {
    t: TranslatorFunc;
}

const ActionButton = ({
    icon = <></>,
    onPress,
    variant = 'dark',
    size = DEFAULT_ACTION_SIZE,
    label,
    color,
    ...props
}: Props): JSX.Element => {
    return (
        <Box flexDirection="row" alignItems="center" justifyContent="flex-end" {...props}>
            {!!label && (
                <Text
                    color={variant === 'dark' ? Theme.colors.white : Theme.colors.primary}
                    fontSize={1}
                    fontWeight={5}
                    padding={0}
                    marginRight={2}
                >
                    {label}
                </Text>
            )}

            <Touchable
                height={size}
                width={size}
                borderRadius={size / 2}
                padding={1}
                alignItems="center"
                justifyContent="center"
                backgroundColor={color ? color : variant === 'dark' ? 'primary' : 'white'}
                borderWidth={variant === 'light' ? 1 / 2 : 0}
                borderColor="primary"
                onPress={onPress}
                style={{
                    shadowColor: Theme.colors.black,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                }}
            >
                {icon}
            </Touchable>
        </Box>
    )
}

export default ActionButton;

ActionButton.Plus = ({ variant = 'dark', size, onPress, ...props }: Props) => {
    const tintColor = variant === 'dark' ? Theme.colors.white : Theme.colors.primary;
    return (
        <ActionButton
            {...{ variant, size, onPress }}
            icon={(
                <Image
                    source={require('../../assets/images/+.png')}
                    style={{
                        tintColor,
                        height: DEFAULT_ACTION_SIZE * 0.45,
                        width: DEFAULT_ACTION_SIZE * 0.45
                    }}
                />
            )}
            {...props}
        />
    )
}

ActionButton.Settings = ({ variant = 'dark', size, onPress, t, ...props }: PropsWithTranslator) => {
    const tintColor = variant === 'dark' ? Theme.colors.white : Theme.colors.primary;
    return (
        <ActionButton
            {...{ variant, size, onPress }}
            icon={(
                <Image
                    source={require('../../assets/images/cog.png')}
                    style={{
                        tintColor,
                        height: DEFAULT_ACTION_SIZE * 0.45,
                        width: DEFAULT_ACTION_SIZE * 0.45
                    }}
                />
            )}
            label={t('navigation.scenes.settings')}
            {...props}
        />
    )
}

ActionButton.Close = ({ variant = 'dark', size, onPress, ...props }: Props) => {
    return (
        <ActionButton
            color={Theme.colors.lightRed}
            {...{ variant, size, onPress }}
            icon={(
                <Image
                    source={require('../../assets/images/x.png')}
                    style={{
                        tintColor: Theme.colors.white,
                        height: DEFAULT_ACTION_SIZE * 0.4,
                        width: DEFAULT_ACTION_SIZE * 0.4,
                    }}
                />
            )}
            {...props}
        />
    )
}

ActionButton.Calendar = ({ variant = 'dark', size, onPress, t, ...props }: PropsWithTranslator) => {
    const tintColor = variant === 'dark' ? Theme.colors.white : Theme.colors.primary;
    return (
        <ActionButton
            {...{ variant, size, onPress }}
            icon={(
                <Image
                    source={require('../../assets/images/calendar.png')}
                    style={{
                        tintColor,
                        height: DEFAULT_ACTION_SIZE * 0.45,
                        width: DEFAULT_ACTION_SIZE * 0.45
                    }}
                />
            )}
            label={t('actions.calendar_create_new')}
            {...props}
        />
    )
}
import React from 'react';

import Box from '../../atoms/Box';
import ActivityIndicator from '../../atoms/ActivityIndicator';

interface Props {
    opacity?: number;
    backgroundColor?: string;
}

const posAbs = { position: 'absolute', top: 0, right: 0, bottom: 0, left: 0 };

const LoadingOverlay = ({ backgroundColor = 'lightGrey', opacity = 0.8 }: Props) => (
    <>
        <Box style={posAbs} {...{ backgroundColor, opacity }} />
        <Box style={posAbs} alignItems="center" justifyContent="center">
            <ActivityIndicator />
        </Box>
    </>
);

export default LoadingOverlay;

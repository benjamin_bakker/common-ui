import React from 'react';
import initials from 'initials';
import { intervalToDuration } from 'date-fns';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

import { BaseBubbleProps, Slot } from '../../types/calendar';

interface Props {
    color?: string;
    start: Slot['start'];
    end: Slot['end'];

    borderColor?: string;

    children: any;
}

const BubbleContainer = (props: any) => <Box padding={'1.5px'} {...props} />;
const BubbleInner = (props: any) => <Box alignSelf="center" width={1} flex={1} borderRadius={5} {...props} />

const SlotBubble = ({ start, end, color, children, borderColor }: Props) => {
    const { hours = 1 } = intervalToDuration({
        start,
        end,
    });

    const innerContainerProps: any = {};
    if (borderColor) {
        innerContainerProps.borderWidth = 1 / 2;
        innerContainerProps.borderColor = borderColor;
    }

    return (
        <BubbleContainer flex={1} height={`${(hours * 10)}%`}>
            <BubbleInner
                alignItems="center"
                justifyContent="center"
                backgroundColor={color || 'primary'}
                {...innerContainerProps}
            >
                {children}
            </BubbleInner>
        </BubbleContainer>
    );
}

export default SlotBubble;

type BaseWithoutOnPressAndChildren = Omit<BaseBubbleProps, 'isFirstBubble'|'onPress'|'children'>;
SlotBubble.External = ({ title, start, end, color = 'grey' }: BaseWithoutOnPressAndChildren&Slot): React.ReactElement => {
    return (
        <SlotBubble color={color} start={start} end={end}>
            <Text color="white" fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{title}</Text>
        </SlotBubble>
    );
};

SlotBubble.Booking = ({ customer, start, end, color = 'primary' }: BaseWithoutOnPressAndChildren&Slot): React.ReactElement => {
    return (
        <SlotBubble start={start} end={end} color={color}>
            {(customer && customer?.displayName) && <Text color="white" fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{initials(customer.displayName)}</Text>}
        </SlotBubble>
    );
};

SlotBubble.BookingRequest = ({ customer, start, end }: BaseWithoutOnPressAndChildren&Slot): React.ReactElement => {
    return (
        <SlotBubble start={start} end={end} color="white" borderColor="primary">
            {(customer && customer?.displayName) && <Text color="primary" fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{initials(customer.displayName)}</Text>}
        </SlotBubble>
    );
};

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  variant,
  position,
} from 'styled-system';

import Theme from '../../theme';

import ActivityIndicator from '../../atoms/ActivityIndicator';
import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';

const buttonVariant = variant({ scale: 'buttons' });
const StyledButton = styled(Touchable)`
  ${buttonVariant}
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${position}
`;

export const buttonTestID = 'button-id';

function Button({
  TextComponent = Text,
  text = '',
  translate = null,
  format = {},
  busy = false,
  iconLeft = null,
  iconRight = null,
  noWrapTheme,
  ...props
}) {
  const activityIndicatorProps = {
    // eslint-disable-next-line react/destructuring-assignment
    variant: `button_${props.variant}`,
    noWrapTheme: true,
  };
  const buttonProps = { ...props };

  // eslint-disable-next-line react/destructuring-assignment
  if (props.variant === 'disabled') {
    buttonProps.disabled = true;
    activityIndicatorProps.variant = 'button_disabled';
  }
  if (busy) {
    buttonProps.disabled = true;
    buttonProps.busy = true;
  }

  const textProps = {
    noWrapTheme: true,
  };
  // eslint-disable-next-line react/destructuring-assignment
  if (props.variant) textProps.variant = `button_${props.variant}`;

  let cursor = 'pointer';
  if (busy) cursor = 'wait';
  // eslint-disable-next-line react/destructuring-assignment
  if (props.variant === 'disabled') cursor = 'not-allowed';
  buttonProps.style = buttonProps.style || {};
  buttonProps.style.cursor = cursor;

  return (
    <ThemeProvider theme={Theme}>
      <StyledButton testID={buttonTestID} {...buttonProps} height="buttonHeight">
        <Box
          noWrapTheme
          flexDirection="row"
          justifyContent="space-evenly"
        >
          <Box noWrapTheme width={7} justifyContent="center">
            {iconLeft}
          </Box>
          <Box noWrapTheme flexGrow={1}>
            <TextComponent translate={translate} format={format} {...textProps}>
              {text}
            </TextComponent>
          </Box>
          <Box noWrapTheme width={7} justifyContent="center">
            {busy ? <ActivityIndicator {...activityIndicatorProps} /> : iconRight}
          </Box>
        </Box>
      </StyledButton>
    </ThemeProvider>
  );
}

Button.defaultProps = {
  a11y: true,
  a11yRole: 'button',
  focusable: true,
  variant: 'primary',
  activeOpacity: 0.9,
  fontSize: 2,
};

export default Button;

import React from 'react';

import Theme from '../../theme';
import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

import { Rule } from '../../types/calendar';
import { TranslatorFunc } from '../../types/global';

interface Props extends Partial<Rule> {
    t: TranslatorFunc;
}

const BookingRuleBanner = ({ name, color = Theme.colors.lightGreen, t }: Props) => {
    if (!name) return null;
    return (
        <Box
            paddingTop={3}
            paddingBottom={3}
            marginRight={2}
            backgroundColor={color}
            borderRadius={5}
            style={{
                shadowColor: Theme.colors.black,
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
            }}
        >
            <Text color="white">{t('text.general.booking_rule')}: {t(`text.rules.${name}`)}</Text>
        </Box>
    );
}

export default BookingRuleBanner;

import React, { useEffect, useState } from 'react';
import { Animated } from 'react-native';

import { ThemeProvider } from 'styled-components/native';
import Theme from '../../theme';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';

export const headerBarContainer = 'header-bar-container';
export const nestedHeaderBarcontainer = 'nested-header-bar-container';

export interface AccordionProps {
  title: string;
  /**
   * @description Props you would pass to a common-ui <Text /> component
   */
  titleProps?: {};
  image: React.ReactElement;
  onOpen?: () => void;
  onClose?: () => void;
  openByDefault?: boolean;
  nested?: boolean;
  children: React.ReactElement|React.ReactElement[];
}

function Accordion({
  title = '',
  titleProps = {},
  image = <></>,
  onOpen = () => {},
  onClose = () => {},
  openByDefault = false,
  nested = false,
  children,
}: AccordionProps): React.ReactElement {
  const [isOpen, setIsOpen] = useState(openByDefault);
  const [chevronPos] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated
      .timing(chevronPos, { toValue: isOpen ? 1 : 0, duration: 300, useNativeDriver: true })
      .start();
  }, [isOpen]);

  function handleToggle() {
    if (isOpen) onClose();
    else onOpen();
    setIsOpen((open) => !open);
  }

  const chevronAngle = chevronPos.interpolate({
    inputRange: [0, 1],
    outputRange: ['90deg', '-90deg'],
  });

  return (
    <ThemeProvider theme={Theme}>
      <Box noWrapTheme>
        <Touchable
          testID={nested ? nestedHeaderBarcontainer : headerBarContainer}
          noWrapTheme
          onPress={handleToggle}
          width={1}
          paddingX={3}
        >
          <Box
            noWrapTheme
            paddingY={3}
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between"
          >
            {image && image}
            <Text
              noWrapTheme
              touchable={false}
              alignSelf="flex-start"
              textAlign="left"
              padding={0}
              flex={0.8}
              fontWeight={isOpen ? 'bold' : 'medium'}
              {...titleProps}
            >
              {title}
            </Text>
            <Animated.Image
              source={require('../../assets/images/chevron.png')}
              style={{
                width: 12,
                height: 12,
                marginRight: nested ? Theme.space[3] : Theme.space[1],
                transform: [{ rotate: chevronAngle }]
              }}
            />
          </Box>
        </Touchable>

        {isOpen && (
          <Box noWrapTheme width={1} height="auto" paddingX={nested ? 3 : 0} paddingBottom={3}>
            {children}
          </Box>
        )}
        {!isOpen && <Box noWrapTheme width={1} height={0.9} backgroundColor="grey" />}
      </Box>
    </ThemeProvider>
  );
}

export default Accordion;

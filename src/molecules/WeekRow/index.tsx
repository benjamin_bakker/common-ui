import React from 'react';

import Theme from '../../theme';
import Box from '../../atoms/Box';

import { CALENDAR_GENERAL_BORDER_WIDTH, MONTH_VIEW_CAL_BORDER_RADIUS, MONTH_VIEW_MAX_WEEKS_SHOWN } from '../../constants/calendar';
import DayCell from '../../molecules/DayCell';

import { Day } from '../../types/calendar';
import { I18nLocale } from '../../types/global';
import { Currency } from '../../types/currency';

export interface Props {
    selectedDay: Date;
    weekNumber: string | number;
    days: Day[];
    view: 'bookings' | 'pricing';
    onViewDay: (day: Day) => any;
    currency: Currency;

    locale: I18nLocale;
}

const WeekRow = ({ selectedDay, days, currency, weekNumber, view = 'bookings', onViewDay, locale }: Props) => {
    const BOX_PROPS: any = {};
    if (+weekNumber === MONTH_VIEW_MAX_WEEKS_SHOWN) {
        BOX_PROPS.borderBottomLeftRadius = MONTH_VIEW_CAL_BORDER_RADIUS;
        BOX_PROPS.borderBottomRightRadius = MONTH_VIEW_CAL_BORDER_RADIUS;
    }

    return (
        <Box
            flexDirection="row"
            flex={1}
            overflow="hidden"
            {...BOX_PROPS}
            style={{
                outlineColor: Theme.colors.lightGrey,
                outlineStyle: "solid",
                outlineWidth: CALENDAR_GENERAL_BORDER_WIDTH,
            }}
        >
            {days.map((dayProps: Day, dayIndex: number) => {
                return (
                    <DayCell
                        key={`week_${weekNumber}_day_${dayIndex}`}
                        locale={locale}
                        selectedDay={selectedDay}
                        view={view}
                        onViewDay={() => onViewDay(dayProps)}
                        dayIndex={dayIndex}
                        currency={currency}
                        {...dayProps}
                    />
                );
            })}
        </Box>
    )
}

export default WeekRow;

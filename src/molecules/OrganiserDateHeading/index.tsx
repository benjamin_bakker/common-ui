import React from 'react';
import { format, isToday } from 'date-fns';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import { TranslatorFunc } from '../../types/global';


interface Props {
    date: Date;

    t: TranslatorFunc;
}

const DateHeading = ({ date, t }: Props) => {
    if (!date) return null;

    return (
        <Box
            padding={2}
            paddingTop={3}
            paddingBottom={3}
            justifyContent="center"
            backgroundColor="white"
            borderBottomWidth={1 / 2}
            borderBottomColor="grey"
        >
            <Text variant="h3" padding={0} margin={0} fontSize={2} color="textGrey">{`${isToday(date) ? `${t('text.general.today')} - ` : ''}${format(date, 'dd/MM/yyyy')}`}</Text>
        </Box>
    );
}

export default DateHeading;

import React from 'react';

import { CALENDAR_GENERAL_BORDER_WIDTH, FALLBACK_WEEK_DAYS, MONTH_VIEW_CAL_BORDER_RADIUS } from '../../constants/calendar';

import { I18nLocale } from '../../types/global';

import { weekDays } from '../../util/calendar';

import Theme from '../../theme';
import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable'
import Text from '../../atoms/Text'

export interface Props {
    onDayPress: (dayIndex: number, dayName: string) => void;
    locale: I18nLocale;
}

const WeekDayHeading = ({ onDayPress, locale }: Props) => {
    let days: string[] = (weekDays[locale] || FALLBACK_WEEK_DAYS).map((day: string) => day.slice(0, 3));

    const sunday = days.pop();
    if (sunday) days = [sunday, ...days];

    return (
        <Box
            flexDirection="row"
            paddingTop={1}
            paddingBottom={1}
            backgroundColor="lightGrey"
            borderTopLeftRadius={MONTH_VIEW_CAL_BORDER_RADIUS}
            borderTopRightRadius={MONTH_VIEW_CAL_BORDER_RADIUS}
            style={{
                outlineColor: Theme.colors.lightGrey,
                outlineStyle: "solid",
                outlineWidth: CALENDAR_GENERAL_BORDER_WIDTH,
            }}
        >
            {days.map((d, i) => (
                <Touchable
                    key={`${d}_${i}`}
                    flex={1}
                    alignItems="center"
                    justifyContent="center"

                    onPress={() => onDayPress(i, d)}
                >
                    <Text fontWeight={5} color="textGrey">{d}</Text>
                </Touchable>
            ))}
        </Box>
    );
}

export default WeekDayHeading;

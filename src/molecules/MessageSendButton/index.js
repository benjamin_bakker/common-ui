import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme';

import Touchable from '../../atoms/Touchable';
import ActivityIndicator from '../../atoms/ActivityIndicator';

export const messageSendButtonTestID = 'message-button';
export const messageSendButtonIconID = 'message-button-icon';
export const messageSendBusyIconTestID = 'message-button-busy-icon';

function MessageSendButton({
  busy,
  disabled,
  width,
  height,
  onPress,
}) {
  return (
    <ThemeProvider theme={Theme}>
      <Touchable
        testID={messageSendButtonTestID}
        noWrapTheme
        width={width}
        height={height}
        borderRadius={width || height}
        alignItems="center"
        justifyContent="center"
        backgroundColor={(disabled || busy) ? Theme.colors.darkGrey : Theme.colors.primary}
        marginRight={3}
        onPress={onPress}
        disabled={disabled || busy}
      >
        {busy ? (
          <ActivityIndicator testID={messageSendBusyIconTestID} noWrapTheme variant="secondary" />
        ) : (
          <Image testID={messageSendButtonIconID} source={require('../../assets/images/send.png')} style={{ width: 15, height: 15, tintColor: Theme.colors.white }} />
        )}
      </Touchable>
    </ThemeProvider>
  );
}

MessageSendButton.defaultProps = {
  busy: false,
  disabled: false,
  width: 30,
  height: 30,
};

MessageSendButton.propTypes = {
  busy: PropTypes.bool,
  disabled: PropTypes.bool,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onPress: PropTypes.func.isRequired,
};

export default MessageSendButton;

import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import SelectBox, {
  selectBoxID,
  selectBoxSelectedValueText,
  selectBoxOptionsContainer,
  selectBoxOption,
  selectBoxOptionTextID,
  selectBoxOptionCheckID,
} from './index';

const options = [{
  value: 'en',
  label: 'English',
}];

const placeholderText = 'Placeholder texteroony';

describe('SelectBox', () => {
  test('@prop: placeholder', () => {
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value=""
        options={options}
        onSelect={() => {}}
      />
    );

    const selectText = getByTestId(selectBoxSelectedValueText);

    expect(selectText.children[0]).toEqual(placeholderText);
  });
  test('@prop: value', () => {
    const selectedOption = options[0];
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value={selectedOption.value}
        options={options}
        onSelect={() => {}}
      />
    );

    const selectText = getByTestId(selectBoxSelectedValueText);

    expect(selectText.children[0]).toEqual(selectedOption.label);
  });
  test('@prop: onSelect', () => {
    const onPress = jest.fn();
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value=""
        options={options}
        onSelect={onPress}
      />
    );

    const selectBox = getByTestId(selectBoxID);
    fireEvent.press(selectBox);

    const option = getByTestId(`${selectBoxOption}_${options[0].value}`);
    fireEvent.press(option);

    expect(onPress).toHaveBeenCalledWith(options[0].value);
  });
  test('Pressing on the SelectBox should toggle the menu.', () => {
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value=""
        options={options}
        onSelect={() => {}}
      />
    );

    const selectBox = getByTestId(selectBoxID);

    expect(() => getByTestId(selectBoxOptionsContainer)).toThrow();

    fireEvent.press(selectBox);

    expect(getByTestId(selectBoxOptionsContainer)).toBeTruthy();

    fireEvent.press(selectBox);

    expect(() => getByTestId(selectBoxOptionsContainer)).toThrow();
  });
  test('Choosing an Option should close the menu.', () => {
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value=""
        options={options}
        onSelect={() => {}}
      />
    );

    const selectBox = getByTestId(selectBoxID);
    fireEvent.press(selectBox);


    const option = getByTestId(`${selectBoxOption}_${options[0].value}`);
    fireEvent.press(option);

    expect(() => getByTestId(selectBoxOptionsContainer)).toThrow();
  });
  test('The selected Option should have a check icon.', () => {
    const selectedValue = options[0].value;
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value={selectedValue}
        options={options}
        onSelect={() => {}}
      />
    );

    const selectBox = getByTestId(selectBoxID);
    fireEvent.press(selectBox);

    const checkMark = getByTestId(`${selectBoxOptionCheckID}_${selectedValue}`);
    expect(checkMark).toBeTruthy();
  });
  test('SelectBox Option should display option.label', () => {
    const selectedOption = options[0];
    const { getByTestId } = render(
      <SelectBox
        placeholder={placeholderText}
        value={selectedOption.value}
        options={options}
        onSelect={() => {}}
      />
    );

    const selectBox = getByTestId(selectBoxID);
    fireEvent.press(selectBox);

    const optionText = getByTestId(`${selectBoxOptionTextID}_${selectedOption.value}`);
    expect(optionText.children[0]).toBe(selectedOption.label);
  });
});

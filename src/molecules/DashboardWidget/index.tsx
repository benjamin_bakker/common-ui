import React, { ReactNode } from 'react';
import { Animated, Image, View } from 'react-native';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';
import Theme from '../../theme';

export const toggleTestID = 'toggle-test-id';
export const headingTestID = 'heading-test-id';
export const subheadingTestID = 'subheading-test-id';
export const iconContainerTestID = 'icon-container-test-id';
export const infoIconTestID = 'info-icon';
export const chevronIconTestID = 'chevron-icon';

type HeadingRender = () => React.ReactElement;
interface WidgetHeaderContainerProps {
  heading: HeadingRender|string;
  subheading?: HeadingRender|string;
  Icon?: React.ReactNode;
  subheadingProps?: {};
  headingProps?: {};

  onPress?: () => void;
}

export interface DashboardWidgetProps extends Omit<WidgetHeaderContainerProps, 'Icon'> {
  containerProps?: {};
  onToggle?: (isOpen: boolean) => void;
  alwaysOpen?: boolean;

  Icon?: (toggleState: boolean) => React.ReactElement;

  children?: ReactNode;
}

const WidgetHeading = (props: any) => (
  <Text testID={headingTestID} variant="h3" color="textGrey" padding={0} fontWeight={0} {...props} />
);

const WidgetSubheading = (props: any) => (
  <Text testID={subheadingTestID} variant="h3" color="textGrey" padding={0} marginTop={2} fontWeight={6} {...props} />
);

const WidgetContainer = (props: any) => <Box padding={2} flex={1} flexGrow={1} {...props} />;
const WidgetInnerContainer = (props: any) => <Box padding={2} borderRadius={10} borderWidth={1/2} borderColor="grey" flexGrow={1} {...props} />;
const WidgetHeaderContainer = ({ Icon, heading, subheading, headingProps, subheadingProps, onPress }: WidgetHeaderContainerProps) => {
  const Container = !!onPress ? Touchable : Box;

  let Heading: HeadingRender|string = () => <></>;
  if (typeof heading === 'string') Heading = () => <WidgetHeading {...headingProps}>{heading}</WidgetHeading>;
  if (typeof heading === 'function') Heading = heading;

  let Subheading: HeadingRender|string = () => <></>;
  if (typeof subheading === 'string') Subheading = () => <WidgetSubheading {...subheadingProps}>{subheading}</WidgetSubheading>;
  if (typeof subheading === 'function') Subheading = subheading;

  return (
    <Container testID={toggleTestID} flexDirection="row" justifyContent="center" flexGrow={1} padding={2} style={{ position: 'relative'}} onPress={onPress}>
      <Box flex={1} justifyContent="flex-start">
        <Heading />
        <Subheading />
      </Box>
      {Icon && (
        <View testID={iconContainerTestID} style={{ position: 'absolute', top: Theme.space[1], right: Theme.space[1] }}>
          {Icon}
        </View>
      )}
    </Container>
  );
}

const DashboardWidget = ({
  heading,
  subheading,
  Icon,
  headingProps,
  subheadingProps,

  containerProps,
  onToggle,
  alwaysOpen,

  children,
}: DashboardWidgetProps): React.ReactElement => {
  const [isOpen, setIsOpen] = React.useState<boolean>(!!alwaysOpen);

  function handleToggle() {
    const currentState = isOpen;
    const nextState = !currentState;
    setIsOpen(nextState);
    onToggle && onToggle(nextState);
  }

  const propsForHeaderContainer: WidgetHeaderContainerProps = {
    heading,
    subheading,
    Icon: Icon ? Icon(isOpen) : null,
    headingProps,
    subheadingProps,
    onPress: (!!children && !alwaysOpen) ? handleToggle : void 0,
  };

  return (
    <WidgetContainer {...containerProps}>
      <WidgetInnerContainer>
        <WidgetHeaderContainer {...propsForHeaderContainer}  />
        {(!!children && isOpen) && (
          <Box padding={2} borderTopWidth={1/2} borderTopColor="grey" flexGrow={9}>
            {children}
          </Box>
        )}
      </WidgetInnerContainer>
    </WidgetContainer>
  );
}

DashboardWidget.ChevronIcon = ({ style = {}, ...props }): React.ReactElement => (
  <Animated.Image
    testID={chevronIconTestID}
    source={require('../../assets/images/chevron.png')}
    style={{
      width: 10,
      height: 10,
      ...style
    }}
    {...props}
  />
);

DashboardWidget.InfoIcon = ({ style = {}, ...props }): React.ReactElement => (
  <Image
    testID={infoIconTestID}
    source={require('../../assets/images/info.png')}
    style={{
      width: 12,
      height: 12,
      tintColor: Theme.colors.black,
      ...style
    }}
    {...props}
  />
);

DashboardWidget.Heading = (props: any): React.ReactElement => <WidgetHeading {...props} />;

DashboardWidget.Subheading = (props: any): React.ReactElement => <WidgetSubheading {...props} />;

export default DashboardWidget;

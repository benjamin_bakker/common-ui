import React from 'react';
import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';

import { BubbleStyleProps } from '.';

interface BubbleContainerProps {
    style?: {};
    height: string|number;
    width: string|number;
    onPress: () => void;

    children: any;

    [key: string]: any;
}
export const BubbleContainer = ({ style = {}, ...props }: BubbleContainerProps) => (
    <Touchable
        alignSelf="flex-end"
        {...props}
        style={{ position: 'absolute', ...style }}
    />
);

interface BubbleInnerProps {
    fullyRounded: BubbleStyleProps['fullyRounded'];
    [key: string]: any;
}
export const BubbleInner = (props: BubbleInnerProps) => {
    let layoutProps: any = { padding: 3, paddingRight: 2, paddingLeft: 4 };
    if (props.fullyRounded) {
        layoutProps = {
            borderBottomRightRadius: 15,
            borderTopRightRadius: 15,
            alignItems: 'center',
        };
    }
    return (
        <Box
            flex={1}
            borderBottomLeftRadius={15}
            borderTopLeftRadius={15}
            {...layoutProps}
            {...props}
        />
    );
}
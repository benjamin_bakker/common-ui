import React from 'react';
import { format, getHours, getMinutes, intervalToDuration } from 'date-fns';

import Box from '../../atoms/Box';
import Theme from '../../theme';
import Text from '../../atoms/Text';
import Avatar from '../Avatar';

import { BubbleContainer, BubbleInner } from './Containers';

import { TIME_BLOCK_HEIGHT, VISIBLE_HOURS } from '../../constants/calendar';

import { BaseBubbleProps, Slot } from '../../types/calendar';

import { getDifferenceInHours, percentageOfHour } from '../../util/calendar';
import { TranslatorFunc } from '../../types/global';

export interface BubbleStyleProps {
    width?: number | string;
    left?: number | string;
    fullyRounded?: boolean;
    borderColor?: string;
}

interface Props extends BubbleStyleProps, BaseBubbleProps {}

const SlotBubble = ({
    onPress,
    start,
    end,
    isFullDay,

    width = 1,
    left = 0,
    fullyRounded = false,
    color = 'primary',
    isFirstBubble,

    borderColor = void 0,

    children,
}: Props) => {
    const { hours, minutes } = intervalToDuration({
        start: start,
        end: end,
    });

    const topOffsetMultiplier = +`${getDifferenceInHours({ hour: getHours(start) })}.${percentageOfHour(getMinutes(start))}`;

    let bubbleHeight = (TIME_BLOCK_HEIGHT * +`${hours}.${percentageOfHour(minutes).toFixed(0)}`);
    if (isFullDay) bubbleHeight = (TIME_BLOCK_HEIGHT * VISIBLE_HOURS.length);

    let top = TIME_BLOCK_HEIGHT * topOffsetMultiplier + 12.45;
    if (isFullDay) top = 12.45;

    const _s: any = { top };
    if (left) _s.left = left;

    const innerContainerProps: any = {};
    if (borderColor) {
        innerContainerProps.borderWidth = 1/2;
        innerContainerProps.borderColor = borderColor;
    }

    return (
        <BubbleContainer
            height={bubbleHeight}
            width={width}
            style={_s}
            onPress={onPress}
            paddingRight={fullyRounded ? Theme.space[1] / 4 : 0}
            paddingLeft={isFirstBubble ? Theme.space[1] / 4 : 0}
        >
            <BubbleInner
                style={{
                    shadowColor: Theme.colors.black,
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                }}
                fullyRounded={fullyRounded}
                backgroundColor={color}
                {...innerContainerProps}
            >
                {children}
            </BubbleInner>
        </BubbleContainer>
    )
}
export default SlotBubble;

interface BookingSlotProps extends Omit<BaseBubbleProps, 'isFullDay' | 'onPress' | 'children'>, Omit<Slot, 'start' | 'end'>, BubbleStyleProps {
    onPress: (slot: Slot) => void;
    small: boolean;

    t: TranslatorFunc;
}

SlotBubble.External = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const fromFormatted = format(slot.start, 'H:mm');
    const toFormatted = format(slot.end, 'H:mm');

    const textProps = {
        color: 'textGrey',
        fontWeight: 5,
        fontSize: small ? 0 : 1,
        padding: 0,
    };

    return (
        <SlotBubble
            color="grey"
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
        >
            <Box marginTop={2}>
                <Text marginBottom={small ? 2 : 0} {...textProps}>{fromFormatted} - {toFormatted}</Text>
                {slot.isFullDay && (
                    <Text fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                )}
                <Text {...textProps} numberOfLines={2} ellipsizeMode="tail">{slot.title}</Text>
            </Box>
        </SlotBubble>
    );
}

SlotBubble.Booking = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const fromFormatted = format(slot.start, 'H:mm');
    const toFormatted = format(slot.end, 'H:mm');

    return (
        <SlotBubble
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
        >
            <Box marginTop={2} flexDirection={small ? 'column' : "row"} alignItems={small ? 'center' : 'flex-start'} justifyContent="space-between">
                <Box justifyContent="space-between">
                    <Text color="white" fontWeight={5} fontSize={small ? 0 : 1} marginBottom={small ? 2 : 0} padding={0}>{fromFormatted} - {toFormatted}</Text>
                    {slot.isFullDay && (
                        <Text color="white" fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                    )}
                    {(!small && !!slot.customer) &&  <Text color="white" fontWeight={5} padding={0}>{slot.customer.displayName}</Text>}
                </Box>

                {!!(slot?.customer && slot.customer.displayName) && (
                    <Box>
                        <Avatar
                            size={small ? 30 : 32.5}
                            fontSize={Theme.fontSizes[1]}
                            name={slot.customer.displayName}
                            email={slot.customer.email}
                            url={slot.customer.avatar?.url}
                        />
                    </Box>
                )}
            </Box>
            {!small && slot?.customer && slot.customer?.phone && (
                <Box flexDirection="row" alignItems="center" marginTop={2}>
                    <Box backgroundColor="grey" borderRadius={5} padding={1} marginRight={3} alignItems="center" justifyContent="center">
                        <Box padding={1} borderRadius={Theme.space[2] / 4} bg="black" />
                    </Box>
                    <Text color="white" fontWeight={5} padding={0} numberOfLines={2} ellipsizeMode="tail">{slot.customer.phone}</Text>
                </Box>
            )}
        </SlotBubble>
    );
}

SlotBubble.BookingRequest = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const fromFormatted = format(slot.start, 'H:mm');
    const toFormatted = format(slot.end, 'H:mm');

    return (
        <SlotBubble
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
            color="white"
            borderColor="primary"
        >
            <Box marginTop={2} flexDirection={small ? 'column' : "row"} alignItems={small ? 'center' : 'flex-start'} justifyContent="space-between">
                <Box justifyContent="space-between">
                    <Text color="primary" fontWeight={5} fontSize={small ? 0 : 1} marginBottom={small ? 2 : 0} padding={0}>{fromFormatted} - {toFormatted}</Text>
                    {slot.isFullDay && (
                        <Text color="primary" fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                    )}
                    {(!small && !!slot.customer) && <Text color="primary" fontWeight={5} padding={0}>{slot.customer.displayName}</Text>}
                </Box>

                <Box>
                    <Avatar size={small ? 30 : 32.5} fontSize={Theme.fontSizes[1]} name={slot?.customer?.displayName} />
                </Box>
            </Box>
            {!small && slot?.customer && slot.customer?.phone && (
                <Box flexDirection="row" alignItems="center" marginTop={2}>
                    <Box backgroundColor="grey" borderRadius={5} padding={1} marginRight={3} alignItems="center" justifyContent="center">
                        <Box padding={1} borderRadius={Theme.space[2] / 4} bg="black" />
                    </Box>
                    <Text color="primary" fontWeight={5} padding={0} numberOfLines={2} ellipsizeMode="tail">{slot.customer.phone}</Text>
                </Box>
            )}
        </SlotBubble>
    );
}

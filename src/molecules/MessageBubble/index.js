import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme';

import { isSingleEmoji } from '../../util/functions';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';

export const messageBubbleTestID = 'message-bubble';
export const messageBubbleContentTestID = 'message-content';
export const messageTimeSentTestID = 'message-time-sent';

const STATE_TO_STYLE_MAP = {
  PRIMARY: {
    backgroundColor: Theme.colors.primary,
    textColor: Theme.colors.white,
  },
  SECONDARY: {
    backgroundColor: Theme.colors.white,
    textColor: Theme.colors.black,
  },
  TERTIARY: {
    backgroundColor: Theme.colors.darkGrey,
    textColor: Theme.colors.white,
  },
  ATTENTION: {
    backgroundColor: Theme.colors.red,
    textColor: Theme.colors.white,
  },
};

function MessageBubble({
  left,
  right,
  timeSent,
  onPress,
  children,
  showTimeSentByDefault,
  state,
}) {
  const [showTimeSent, setShowTimeSent] = useState(showTimeSentByDefault);

  let color = 'convoBlue';
  let maxWidth = 1;
  let width = 1;
  let alignSelf = 'flex-start';
  let textAlign = 'center';
  let textColor = 'black';
  let fontWeight = 4;
  if (left) {
    color = 'convoGrey';
    maxWidth = '75%';
    width = 'auto';
    textAlign = 'left';
    fontWeight = 3;
  }
  if (right) {
    color = 'primary';
    maxWidth = '75%';
    width = 'auto';
    alignSelf = 'flex-end';
    textAlign = 'left';
    textColor = 'white';
    fontWeight = 3;
  }
  if (state) {
    color = STATE_TO_STYLE_MAP[state].backgroundColor;
    textColor = STATE_TO_STYLE_MAP[state].textColor;
  }

  return (
    <ThemeProvider theme={Theme}>
      <Box noWrapTheme padding={2} width={1}>
        <Touchable
          testID={messageBubbleTestID}
          noWrapTheme
          width={width}
          maxWidth={maxWidth}
          padding={3}
          backgroundColor={color}
          borderRadius={10}
          alignSelf={alignSelf}
          onPress={() => {
            if (left || right) {
              setShowTimeSent(!showTimeSent);
            }

            if (onPress) {
              onPress();
            }
          }}
        >
          <Text
            testID={messageBubbleContentTestID}
            touchable
            noWrapTheme
            color={textColor}
            fontSize={isSingleEmoji(children) ? 5 : 1}
            textAlign={textAlign}
            fontWeight={fontWeight}
          >
            {children}
          </Text>
        </Touchable>
        {showTimeSent && (
          <Text testID={messageTimeSentTestID} noWrapTheme fontSize="10px" fontWeight={3} alignSelf={alignSelf} marginTop={1} marginX={2}>
            {timeSent}
          </Text>
        )}
      </Box>
    </ThemeProvider>
  );
}

MessageBubble.defaultProps = {
  left: undefined,
  right: undefined,
  timeSent: '',
  showTimeSentByDefault: false,
  onPress: () => {},
  state: undefined,
};

MessageBubble.propTypes = {
  left: PropTypes.bool,
  right: PropTypes.bool,
  timeSent: PropTypes.string,
  showTimeSentByDefault: PropTypes.bool,
  onPress: PropTypes.func,
  state: PropTypes.string,
};

export default MessageBubble;

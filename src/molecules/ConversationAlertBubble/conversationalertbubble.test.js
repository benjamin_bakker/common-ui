/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { render } from '@testing-library/react-native';

import Theme from '../../theme';

import ConversationAlertBubble, {
  conversationAlertBubbleTestID,
  conversationAlertBubbleContentTestID,
} from './index';

describe('MessageBubble', () => {
  test('Renders correctly', () => {
    const text = 'test text';
    const element = render(<ConversationAlertBubble text={text} />);

    const tree = element.toJSON();
    expect(tree).toMatchSnapshot();

    const node = element.getByTestId(conversationAlertBubbleTestID);
    expect(node).toBeTruthy();
  });

  test(`backgroundColor should be ${Theme.colors.lightRed}`, () => {
    const { getByTestId } = render(<ConversationAlertBubble text="test" />);

    const element = getByTestId(conversationAlertBubbleTestID);
    expect(element.props.style[0].backgroundColor).toBe(Theme.colors.lightRed);
  });

  test('prop text displayed', () => {
    const text = 'some warning text';
    const { getByTestId } = render(<ConversationAlertBubble text={text} />);

    const textElement = getByTestId(conversationAlertBubbleContentTestID);

    expect(textElement).toHaveTextContent(text);
  });
});

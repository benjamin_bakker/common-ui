# Maisonsport Common UI
A Native-first suite of UI Components to be consumed by React-Native & Web (React-Native for Web)

## Installation
```
npm install maisonsport-common-ui
```

## Building and Packaging
If you wish to build and pack this library so you can test integration with another
service, you can run the below:

```
npm pack
```

## Releasing
Once you are ready to release, do not manually update the package json version and instead,
checkout the master branch, pull, and run `npm version <VERSION_TYPE>`. Where version type
is either `major`, `minor` or `patch`.

Then run: `npm publish`

Ensure you also tag the head of master in Gitlab to the version displayed by the above command.

You will have an updated package json locally which you will need to raise a PR for to update in master.

## Contribution

This library should only contain common components that are likely to be reused 
throughout services.

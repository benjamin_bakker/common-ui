/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';

import Theme from '../src/theme';

import MessageBubble, {
  messageBubbleTestID,
  messageBubbleContentTestID,
  messageTimeSentTestID,
} from '../src/molecules/MessageBubble';

const commonProps = {
  timeSent: 'Just now',
};

describe('MessageBubble', () => {
  test('Children of MessageBubble should appear in the content Text Node', () => {
    const testContent = 'Testing testing 123';
    const { getByTestId } = render(<MessageBubble {...commonProps}>{testContent}</MessageBubble>);
    const messageBubbleContentNode = getByTestId(messageBubbleContentTestID);

    expect(messageBubbleContentNode.props.children).toBe(testContent);
  });

  test('onPress prop should be called when Message Bubble is pressed', () => {
    const onPressSpy = jest.fn();
    const { getByTestId } = render(<MessageBubble {...commonProps} onPress={onPressSpy} />);

    const el = getByTestId(messageBubbleTestID);
    fireEvent.press(el);

    expect(onPressSpy).toHaveBeenCalled();
  });

  test('When the Bubble text-content is pressed - the event itself should "bubble" to the main Bubble-container', () => {
    const onPressSpy = jest.fn();
    const { getByTestId } = render(<MessageBubble {...commonProps} onPress={onPressSpy} />);

    const el = getByTestId(messageBubbleContentTestID);
    fireEvent.press(el);

    expect(onPressSpy).toHaveBeenCalled();
  });

  test(`left prop should render the message bubble background color as ${Theme.colors.convoGrey}`, () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} left />);
    const el = getByTestId(messageBubbleTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.convoGrey);
  });

  test(`right prop should render the message bubble background color as ${Theme.colors.convoPink}`, () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} right />);
    const el = getByTestId(messageBubbleTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.primary);
  });

  test(`Message bubble background color by default should be ${Theme.colors.convoBlue}`, () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} />);
    const el = getByTestId(messageBubbleTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.convoBlue);
  });

  test('Message bubble alignSelf style should be flex-start by default', () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} />);
    const el = getByTestId(messageBubbleTestID);

    expect(el.props.style.alignSelf).toBe('flex-start');
  });

  test('right prop should render Message bubble alignSelf style as flex-end', () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} right />);
    const el = getByTestId(messageBubbleTestID);

    expect(el.props.style.alignSelf).toBe('flex-end');
  });

  test('timeSent node not be visible by default', async () => {
    const { queryByTestId } = render(<MessageBubble {...commonProps} />);
    const timeSentNode = queryByTestId(messageTimeSentTestID);

    expect(timeSentNode).toBeNull();
  });

  test('timeSent node should be visible on bubble by default if prop showTimeSentByDefault is true', async () => {
    const { findByTestId } = render(<MessageBubble {...commonProps} showTimeSentByDefault />);

    const timeSentNode = await findByTestId(messageTimeSentTestID);
    expect(timeSentNode).toBeTruthy();
  });

  test('right prop should render Message timeSent node with alignSelf style as flex-end', async () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} right />);
    const bubble = getByTestId(messageBubbleTestID);

    fireEvent.press(bubble);

    const timeSentNode = await waitFor(() => getByTestId(messageTimeSentTestID));
    expect(timeSentNode.props.style[0].alignSelf).toBe('flex-end');
  });

  test('Message bubble content textAlign should be center by default', () => {
    const { getByTestId } = render(<MessageBubble {...commonProps} />);
    const el = getByTestId(messageBubbleContentTestID);

    expect(el.props.style[0].textAlign).toBe('center');
  });

  test('left|right prop should render Message bubble maxWidth of 75%', () => {
    const render1 = render(<MessageBubble {...commonProps} left />);
    const render2 = render(<MessageBubble {...commonProps} right />);
    const el1 = render1.getByTestId(messageBubbleTestID);
    const el2 = render2.getByTestId(messageBubbleTestID);

    expect(el1.props.style.maxWidth).toBe('75%');
    expect(el2.props.style.maxWidth).toBe('75%');
  });

  test('A single emoji should render as large text', () => {
    // eslint-disable-next-line jsx-a11y/accessible-emoji
    const { getByTestId } = render(<MessageBubble {...commonProps}>😀</MessageBubble>);
    const el = getByTestId(messageBubbleContentTestID);

    expect(el.props.style[0].fontSize).toBe(Theme.fontSizes[5]);
  });

  test('A single with text should render as normal text', () => {
    // eslint-disable-next-line jsx-a11y/accessible-emoji
    const { getByTestId } = render(<MessageBubble {...commonProps}>😀 hello</MessageBubble>);
    const el = getByTestId(messageBubbleContentTestID);

    expect(el.props.style[0].fontSize).toBe(Theme.fontSizes[1]);
  });
});

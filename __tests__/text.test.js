import React from 'react';
import renderer from 'react-test-renderer';

import Text from '../src/atoms/Text';
import Theme from '../src/theme';

describe('Text', () => {
  test('References theme.space correctly', () => {
    const tree = renderer.create(<Text marginLeft={1} />).toJSON();
    const [styles] = tree.props?.style || {};

    expect(styles.marginLeft).toBe(Theme.space[1]);
  });

  test('References theme.color correctly', () => {
    const tree = renderer.create(<Text color="navy" />).toJSON();
    const [styles] = tree.props?.style || {};

    expect(styles.color).toBe(Theme.colors.navy);
  });

  test('pointerEvents should be none by default', () => {
    const tree = renderer.create(<Text />).toJSON();

    expect(tree.props.pointerEvents).toBe('none');
  });

  test('pointerEvents should be undefined if the prop touchable is present', () => {
    const tree = renderer.create(<Text touchable />).toJSON();

    expect(tree.props.pointerEvents).toBeUndefined();
  });

  test('a11yLabel should be undefined if the prop a11y is not present', () => {
    const tree = renderer.create(<Text a11yLabel="Some label text" />).toJSON();

    expect(tree.props.accessibilityLabel).toBeUndefined();
  });

  test('a11yLabel should be translated to accessibilityLabel', () => {
    const labelText = 'Label text';
    const tree = renderer.create(<Text a11y a11yLabel={labelText} />).toJSON();

    expect(tree.props.accessibilityLabel).toBe(labelText);
  });
});

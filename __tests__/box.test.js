import React from 'react';
import { render } from '@testing-library/react-native';

import Box from '../src/atoms/Box';

describe('Box', () => {
  test('Renders correctly', () => {
    const tree = render(<Box />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Without prop { onPress } - Box should have prop pointerEvents of "box-none"', () => {
    const tree = render(<Box />).toJSON();
    expect(tree.props.pointerEvents).toBe('box-none');
  });

  test('a11yLabel should be converted to accessibilityLabel', () => {
    const labelText = 'Label text';
    const { getByA11yLabel } = render(<Box a11y a11yLabel={labelText} />);
    expect(getByA11yLabel(labelText)).toBeTruthy();
  });

  test('prop { disabled } should be added into accessibilityStates', () => {
    const tree = render(<Box a11y disabled />).toJSON();
    expect(tree.props.accessibilityStates).toContain('disabled');
  });

  test('prop { a11yRole } or button should be added to accessibilityRole', () => {
    const tree = render(<Box a11y a11yRole="button" />).toJSON();
    expect(tree.props.accessibilityRole).toContain('button');
  });
});

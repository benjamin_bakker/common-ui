import React from 'react';
import { render } from '@testing-library/react-native';

import Scrollable from '../src/atoms/Scrollable';

describe('Scrollable', () => {
  test('Renders correctly', () => {
    const tree = render(<Scrollable />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('a11yLabel should be converted to accessibilityLabel', () => {
    const labelText = 'Label text';
    const { getByA11yLabel } = render(<Scrollable a11y a11yLabel={labelText} />);
    expect(getByA11yLabel(labelText)).toBeTruthy();
  });

  test('prop { disabled } should be added into accessibilityStates', () => {
    const tree = render(<Scrollable a11y disabled />).toJSON();
    expect(tree.props.accessibilityStates).toContain('disabled');
  });

  test('prop { a11yRole } or button should be added to accessibilityRole', () => {
    const tree = render(<Scrollable a11y a11yRole="button" />).toJSON();
    expect(tree.props.accessibilityRole).toContain('button');
  });

  test('should render parent as RCTScrollView', () => {
    const tree = render(<Scrollable />).toJSON();
    expect(tree.type).toBe('RCTScrollView');
  });
});

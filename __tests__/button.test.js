import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react-native';

import Button, { buttonTestID } from '../src/molecules/Button';

describe('Button', () => {
  test('Renders correctly', () => {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('When pressed should call onPress callback', () => {
    const onPressMock = jest.fn();
    const { getByTestId } = render(<Button onPress={onPressMock} />);
    fireEvent.press(getByTestId(buttonTestID));

    expect(onPressMock).toHaveBeenCalled();
  });
});

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react-native';

import Avatar, { avatarContainerTestID, avatarContentTestID, avatarImageTestID } from '../src/molecules/Avatar';

const commonProps = {
  name: 'Test User',
};

describe('Avatar', () => {
  test('Renders correctly', () => {
    const tree = renderer.create(<Avatar {...commonProps} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('name prop of two names should render as the first initials of each name', () => {
    const { getByTestId } = render(<Avatar {...commonProps} />);

    const el = getByTestId(avatarContentTestID);
    expect(el.props.children).toBe('TU');
  });

  test('borderRadius of avatar container should be same as size prop', () => {
    const size = 100;
    const { getByTestId } = render(<Avatar {...commonProps} size={size} />);

    const el = getByTestId(avatarContainerTestID);
    expect(el.props.style[0].borderRadius).toBe(size);
  });

  test('with no url prop and no email prop, the image node should not be present', () => {
    const { queryByTestId } = render(<Avatar {...commonProps} />);

    const el = queryByTestId(avatarImageTestID);
    expect(el).toBeNull();
  });

  test('with a url prop, the image node should be present', () => {
    const { queryByTestId } = render(<Avatar {...commonProps} url="https://i.picsum.photos/id/861/200/200.jpg?hmac=UJSK-tjn1gjzSmwHWZhjpaGahNSBDQWpMoNvg8Bxy8k" />);

    const el = queryByTestId(avatarImageTestID);
    expect(el).toBeTruthy();
  });

  test('with an email prop, the image node should be present', () => {
    const { queryByTestId } = render(<Avatar {...commonProps} email="foo@bar.test" />);

    const el = queryByTestId(avatarImageTestID);
    expect(el).toBeTruthy();
  });
});

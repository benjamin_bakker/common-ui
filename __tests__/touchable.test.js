import React from 'react';
import { render } from '@testing-library/react-native';

import Touchable from '../src/atoms/Touchable';

describe('Touchable', () => {
  test('Renders correctly', () => {
    const tree = render(<Touchable />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('a11yLabel should be converted to accessibilityLabel', () => {
    const labelText = 'Label text';
    const { getByA11yLabel } = render(<Touchable a11y a11yLabel={labelText} />);
    expect(getByA11yLabel(labelText)).toBeTruthy();
  });

  // test('prop { disabled } should be added into accessibilityStates', () => {
  //   const tree = render(<Touchable a11y disabled />).toJSON();
  //   expect(tree.props.accessibilityStates).toContain('disabled');
  // });

  test('prop { a11yRole } of button should be added to accessibilityRole', () => {
    const tree = render(<Touchable a11y a11yRole="button" />).toJSON();
    expect(tree.props.accessibilityRole).toContain('button');
  });

  test('prop { onPress } should render parent with touch events', () => {
    const tree = render(<Touchable onPress={() => {}} />).toJSON();
    expect(tree.props.onClick).toBeInstanceOf(Function);
  });
});

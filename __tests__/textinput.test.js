import React from 'react';
import { View, Text } from 'react-native';
import {
  act, fireEvent, render, waitFor,
} from '@testing-library/react-native';

import Theme from '../src/theme';

import TextInput, {
  inputID,
  errorMessageTestID,
  placeholderErrorIconID,
  inputContainerID,
  inputIconID,
  inputColors,
  inputMargins,
  inputParentID,
} from '../src/molecules/TextInput';

describe('TextInput', () => {
  it('Calls onChangeText prop', () => {
    const onChangeSpy = jest.fn();
    const { getByTestId } = render(<TextInput value="Test value" onChangeText={onChangeSpy} />);

    act(() => {
      fireEvent.changeText(getByTestId(inputID), 'next value');
    });

    expect(onChangeSpy).toHaveBeenCalledWith('next value');
  });

  it('prop { icon } render icon Component', async () => {
    const Icon = () => <View />;
    const { findByTestId } = render(<TextInput value="Test value" icon={<Icon />} />);

    const iconElement = await findByTestId(inputIconID);
    expect(iconElement).toBeTruthy();
  });

  it('props { error, errorMessages } cause render of errorMessage', async () => {
    const errorMessage = 'Woops, Error!';
    const { getAllByTestId, findAllByTestId } = render(
      <TextInput error errorMessages={[errorMessage]} />,
    );

    const errorElements = await findAllByTestId(errorMessageTestID);
    expect(errorElements.length).toBe(1);
    expect(errorElements[0]).toBeTruthy();
    expect(getAllByTestId(errorMessageTestID)[0].props.children).toBe(errorMessage);
  });

  it('prop { error } alone does not render errorMessage', async () => {
    const { queryAllByTestId } = render(<TextInput error />);

    await waitFor(() => expect(queryAllByTestId(errorMessageTestID)[0]).not.toBeTruthy());
  });

  it('prop { error } alone renders placeholder ErrorIconComponent', async () => {
    const { queryByTestId } = render(<TextInput error />);

    await waitFor(() => expect(queryByTestId(placeholderErrorIconID)).toBeTruthy());
  });

  it('props { error, ErrorIconComponent } renders ErrorIconComponent', async () => {
    const errorIconID = 'iconID';
    const customErrorIconContent = '!error!';
    const ErrorIcon = () => <Text testID={errorIconID}>{customErrorIconContent}</Text>;
    const {
      getByTestId,
      queryByTestId,
    } = render(<TextInput error ErrorIconComponent={ErrorIcon} />);

    await waitFor(() => expect(queryByTestId(errorIconID)).toBeTruthy());
    expect(getByTestId(errorIconID).props.children).toBe(customErrorIconContent);
  });

  it('prop { error } causes appropriate styling on container', async () => {
    const {
      getByTestId,
    } = render(<TextInput error />);

    const container = getByTestId(inputContainerID);
    const [style] = container.props.style;

    expect(style.borderColor).toBe(Theme.colors[inputColors.error]);
    expect(style.marginBottom).toBe(Theme.space[inputMargins.error]);
  });

  it('prop { errorMessages } should ignore falsey entries', async () => {
    const { getAllByTestId } = render(
      <TextInput
        errorMessages={['render me', null, undefined, 0, '']}
        onChangeText={() => {}}
      />,
    );

    const errorElements = await getAllByTestId(errorMessageTestID);
    expect(errorElements.length).toBe(1);
  });

  it('prop { placeholder } should be passed to parent testID', async () => {
    const { queryByTestId } = render(
      <TextInput
        placeholder="footestbar"
        onChangeText={() => {}}
      />,
    );

    const parent = await queryByTestId(`footestbar_${inputParentID}`);
    expect(parent).toBeTruthy();
  });
});

import React from 'react';
import { Platform } from 'react-native';
import renderer from 'react-test-renderer';

import KeyboardAvoidingView, { verticalOffsets, behaviors } from '../src/atoms/KeyboardAvoidingView';

describe('KeyboardAvoidingView - Android', () => {
  it('Renders correctly', () => {
    Platform.OS = 'android';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`behavior prop is ${behaviors.android}`, () => {
    Platform.OS = 'android';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree.props.testBehavior).toBe(behaviors.android);
  });

  it(`keyboardVerticalOffset prop is ${verticalOffsets.android}`, () => {
    Platform.OS = 'android';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree.props.testKeyboardVerticalOffset).toBe(verticalOffsets.android);
  });
});

describe('KeyboardAvoidingView - IOS', () => {
  it('Renders correctly', () => {
    Platform.OS = 'ios';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`behavior prop is ${behaviors.ios}`, () => {
    Platform.OS = 'ios';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree.props.testBehavior).toBe(behaviors.ios);
  });

  it(`keyboardVerticalOffset prop is ${verticalOffsets.ios}`, () => {
    Platform.OS = 'ios';
    const tree = renderer.create(<KeyboardAvoidingView />).toJSON();
    expect(tree.props.testKeyboardVerticalOffset).toBe(verticalOffsets.ios);
  });
});

/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import renderer from 'react-test-renderer';
import { render, waitFor, fireEvent } from '@testing-library/react-native';

import Theme from '../src/theme';

import MessageSendButton, {
  messageSendButtonTestID,
  messageSendButtonIconID,
  messageSendBusyIconTestID,
} from '../src/molecules/MessageSendButton';

const commonProps = {
  onPress: () => {},
};

describe('MessageSendButton', () => {
  test('Renders correctly', () => {
    const tree = renderer.create(<MessageSendButton {...commonProps} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('button renders with send-icon by default', async () => {
    const { queryByTestId } = render(<MessageSendButton {...commonProps} />);

    await waitFor(() => expect(queryByTestId(messageSendButtonIconID)).toBeTruthy());
  });

  test('button with prop busy renders with busy icon', async () => {
    const { queryByTestId } = render(<MessageSendButton {...commonProps} busy />);

    await waitFor(() => expect(queryByTestId(messageSendBusyIconTestID)).toBeTruthy());
  });

  test('button with prop busy should not fire onPress callback', () => {
    const onPressSpy = jest.fn();
    const { getByTestId } = render(
      <MessageSendButton
        {...commonProps}
        onPress={onPressSpy}
        busy
      />,
    );

    const el = getByTestId(messageSendButtonTestID);
    fireEvent.press(el);

    expect(onPressSpy).not.toHaveBeenCalled();
  });

  test(`disabled|busy props should render container backgroundColor as ${Theme.colors.darkGrey}`, () => {
    const { getByTestId, rerender } = render(<MessageSendButton {...commonProps} busy />);
    let el = getByTestId(messageSendButtonTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.darkGrey);

    rerender(<MessageSendButton {...commonProps} disabled />);
    el = getByTestId(messageSendButtonTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.darkGrey);
  });

  test(`Container backgroundColor ${Theme.colors.primary} by default`, () => {
    const { getByTestId } = render(<MessageSendButton {...commonProps} />);
    const el = getByTestId(messageSendButtonTestID);

    expect(el.props.style.backgroundColor).toBe(Theme.colors.primary);
  });
});
